#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cassert>
#include <cmath>
#include <cctype>
#include <climits>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;

const uint16_t ENDIAN_LITTLE = 0x4949;
const uint16_t ENDIAN_BIG    = 0x4d4d;

#endif /* __PROGTEST__ */

class BinaryImage {

    struct BINARY_HEADER {

        uint16_t encoding;
        uint16_t width;
        uint16_t height;
        short channel_number;
        short data_encoding;
    };

    private:

        BINARY_HEADER head;

        char * data;

        int body_length;

        bool parseHeader(const char * header);

        bool validateDataFormat(uint16_t data_format);

        bool validateDataSize();

    public:

        BinaryImage()
        {
            this->body_length = 0;
        }

        bool openFile ( const char * filename)
        {

            ifstream inp_file(filename, ios::binary);

            if( inp_file.fail() || !inp_file.is_open() || !(inp_file.good()) )
            {
                inp_file.close();
                return false;
            }

            //check size
            inp_file.seekg(0, inp_file.end);

            int file_size = inp_file.tellg();
            file_size = file_size-8;

            inp_file.seekg(0, inp_file.beg);

            if(file_size < 9)
            {
                inp_file.close();
                return false;
            }

            // HEADER
            char * header = new char[8];

            inp_file.read(header,8);

            if(!parseHeader(header))
            {
                delete [] header;
                inp_file.close();

                return false;
            }

            delete [] header;

            //BODY

            inp_file.seekg(0, inp_file.end);

            this->body_length = inp_file.tellg();
            this->body_length = this->body_length-8;

            inp_file.seekg(8, inp_file.beg);

            if(!validateDataSize())
            {
                inp_file.close();
                return false;
            }

            this->data = new char[this->body_length];

            inp_file.read(data,this->body_length);

            int bytesize = (( ((this->head.channel_number == 3) ? 8 : 16) * ((this->head.data_encoding == 0) ? 1 : ( this->head.data_encoding == 2 ) ? 3 : 4 ) )/ 8);

            if( (this->head.width)*(this->head.height)*(bytesize) != this->body_length )
            {
                inp_file.close();
                return false;
            }

            inp_file.close();

            return true;
        }

        void flipVertical();

        void flipHorizontal();

        bool saveImage(const char * filename);

};

bool BinaryImage::parseHeader(const char * header)
{
    //--- ENDIAN_ENCODING --- //
    uint8_t endian_encoding_1 = (uint8_t)(header[0]);
    uint8_t endian_encoding_2 = (uint8_t)(header[1]);

    //--- WIDTH -----//
    uint8_t width_1           = (uint8_t)(header[2]);
    uint8_t width_2           = (uint8_t)(header[3]);

    //--- HEIGHT --- //
    uint8_t height_1          = (uint8_t)(header[4]);
    uint8_t height_2          = (uint8_t)(header[5]);

    //--- DATA FORMATTING -- //
    uint8_t data_format_1 = header[6];
    uint8_t data_format_2 = header[7];
    uint16_t data_format;


    this->head.encoding = ((endian_encoding_1 << 8) | endian_encoding_2);

    if( this->head.encoding == ENDIAN_LITTLE )
    {

        this->head.width           = ((width_2 << 8) | width_1);
        this->head.height          = ((height_2 << 8) | height_1);
        data_format     = ((data_format_2 << 8) | data_format_1 );

    } else {

        this->head.width           = ((width_1 << 8) | width_2);
        this->head.height          = ((height_1 << 8) | height_2);
        data_format                = ((data_format_1 << 8) | data_format_2);
    }

    if(this->head.encoding != ENDIAN_LITTLE && this->head.encoding != ENDIAN_BIG)
    {

        return false;
    }

    if( this->head.width <= 0 || this->head.height <= 0 )
    {
        return false;
    }


    if( !validateDataFormat(data_format) )
    {

        return false;
    }

    return true;
}


 bool BinaryImage::validateDataFormat(uint16_t data_format)
{

    if( data_format > 0x1F )
    {
        return false;
    }

    this->head.channel_number =  (data_format >> 2);
    this->head.data_encoding  =  (((data_format >> 2) << 2) ^ data_format);

    //3 bit
    if(     this->head.channel_number == 1
            ||
            this->head.channel_number ==  2
            ||
            this->head.channel_number == 5
            ||
            this->head.channel_number == 6
            ||
            this->head.channel_number == 7
            )
    {
        return false;
    }

    // 2 bit
    if(this->head.data_encoding == 1)
    {
        return false;
    }

    return true;
}


bool BinaryImage::validateDataSize(  )
{
    //channel_number -- tells how many bits contains one channel
    //data_encoding  -- tells how many channels contains one pixel, so in the end data_encoding tells how many bytes I have to take from file

    switch (this->head.channel_number)
    {
        // 0000
        case 0:
            // 1 bit for channel - how many channels are there

            switch ( this->head.data_encoding ) {

                //black/white - one channel, pixel is made of 1 pixel
                case 0:
                    return ((this->head.width*this->head.height) <= this->body_length*8);

                    //rgb - three channels, pixel is made of 1*3 pixels
                case 2:
                    return ((this->head.width*this->head.height) <= ( (this->body_length*8) / 3) );

                    //rgba - four channels, pixel is made of 1*4
                case 3:
                    return ((this->head.width*this->head.height) <= ( (this->body_length*8) / 4) );

            }


            //011
        case 3:

            //8 bits ( 1 byte ) for channels - how many channels are there
            switch ( this->head.data_encoding ) {

                //black/white - one channel, pixel is made of 8 pixel
                case 0:
                    return ( (this->head.width*this->head.height) <= (this->body_length*8)/8 );

                    //rgb - three channels, pixel is made of 8*3 pixels
                case 2:
                    return ((this->head.width*this->head.height) <= ( (this->body_length*8) / (8*3)) );

                    //rgba - four channels, pixel is made of 1*4
                case 3:
                    return ((this->head.width*this->head.height) <= ( (this->body_length*8) / (8*4)) );

            }

            //100
        case 4:

            //16 bits ( 2 byte ) for channel - how many channels are there
            switch ( this->head.data_encoding ) {

                //black/white - one channel, pixel is made of 8 pixel
                case 0:
                    return ( (this->head.width*this->head.height) <= (this->body_length*8)/8 );

                    //rgb - three channels, pixel is made of 8*3 pixels
                case 2:
                    return ((this->head.width*this->head.height) <= ( (this->body_length*8) / (16*3)) );

                    //rgba - four channels, pixel is made of 1*4
                case 3:
                    return ((this->head.width*this->head.height) <= ( (this->body_length*8) / (16*4)) );
            }
    }

    return false;

}

void BinaryImage::flipVertical()
{

    char * flipped = new char[this->body_length];

    int bytesize = ( ((this->head.channel_number == 3) ? 8 : 16) * ((this->head.data_encoding == 0) ? 1 : ( this->head.data_encoding == 2 ) ? 3 : 4 ) )/ 8;

    int pos = 0;

    for(int h = 0; h < (this->body_length/(bytesize*this->head.width)); h++)
    {
        for(int w = (bytesize*this->head.width - 1); w >= 0; w=w-bytesize)
        {
            for(int b = bytesize-1; b >= 0; b--)
            {
                flipped[pos] = this->data[(h*(bytesize*this->head.width)) + w - b];
                pos++;
            }

        }

    }

    this->data = flipped;

    flipped = nullptr;

}

void BinaryImage::flipHorizontal()
{

    char * flipped = new char[this->body_length];

    int bytesize = ( ((this->head.channel_number == 3) ? 8 : 16) * ((this->head.data_encoding == 0) ? 1 : ( this->head.data_encoding == 2 ) ? 3 : 4 ) )/ 8;

    int pos = 0;

    for(int i = (( this->body_length / (bytesize*(this->head.width)) ) - 1 ) ; i >= 0; i-- )
    {
        for(int s = 0 ; s < (bytesize*(this->head.width) ); s++)
        {
            flipped[pos] = this->data[ (i*(bytesize*(this->head.width))) + s ];

            pos++;
        }
    }

    this->data = flipped;

    flipped = nullptr;
}


bool BinaryImage::saveImage(const char *filename)
{

    ofstream output_f( filename, ios::out | ios::binary );

    if( output_f.fail() || !(output_f.is_open()) || !(output_f.good()) )
    {
        output_f.close();
        return false;
    }

    char header[8];

    //save header
    header[0] = (this->head.encoding >> 8 ) ;
    header[1] = (((this->head.encoding >> 8 ) << 8 ) ^ this->head.encoding) ;

    if( this->head.encoding == ENDIAN_LITTLE )
    {
        header[3] = (this->head.width >> 8 ) ;
        header[2] = (((this->head.width >> 8 ) << 8 ) ^ this->head.width) ;

        header[5] = (this->head.height >> 8 ) ;
        header[4] = (((this->head.height >> 8 ) << 8 ) ^ this->head.height) ;


        header[6] = ((this->head.channel_number << 2) | this->head.data_encoding) ;
        header[7] = 00000000;

    } else {

        header[2] = (this->head.width >> 8 ) ;
        header[3] = (((this->head.width >> 8 ) << 8 ) ^ this->head.width) ;

        header[4] = (this->head.height >> 8 ) ;
        header[5] = (((this->head.height >> 8 ) << 8 ) ^ this->head.height) ;


        header[7] = ((this->head.channel_number << 2) | this->head.data_encoding) ;
        header[6] = 00000000;
    }

    output_f.write(header, 8);

    if(output_f.fail())
    {
        output_f.close();
        return false;
    }

    output_f.write(this->data, this->body_length);

    if(output_f.fail())
    {
        output_f.close();
        return false;
    }

    output_f.close();

    return true;
}

bool flipImage ( const char  * srcFileName,
                 const char  * dstFileName,
                 bool          flipHorizontal,
                 bool          flipVertical ) {

    BinaryImage inpt;

    if (!inpt.openFile(srcFileName)) {

        return false;
    }

    if (flipVertical == 1) {

        inpt.flipHorizontal();
    }


    if (flipHorizontal == 1) {

        inpt.flipVertical();
    }


    if(!inpt.saveImage(dstFileName)) {

        return false;
    }

    return true;

}

#ifndef __PROGTEST__

//----------------------------

bool identicalFiles ( const char * fileName1,
                      const char * fileName2 ) {

    ifstream file1(fileName1, ios::binary);
    ifstream file2(fileName2, ios::binary);

    //file1
    if(file1.fail() || !(file1.is_open()) || file2.fail() || !(file2.is_open()) )
    {
        return false;
    }

    file1.seekg(0, file1.end);
    int length_1 = file1.tellg();
    file1.seekg(0,file1.beg);

    char * data_1 = new char[length_1];

    file1.read(data_1, length_1);

    //--------

    //file2

    file2.seekg(0, file2.end);
    int length_2 = file2.tellg();
    file2.seekg(0,file2.beg);

    if(length_1 != length_2 )
    {

        delete [] data_1;

        file1.close();
        file2.close();

        return false;
    }


    char * data_2 = new char[length_2];

    file2.read(data_2, length_2);

    //--------

    for(int i = 0; i < length_1; i++ )
    {
        if(data_1[i] != data_2[i])
        {

            delete [] data_2;
            delete [] data_1;

            file1.close();
            file2.close();

            return false;
        }

    }

    delete [] data_2;
    delete [] data_1;

    file1.close();
    file2.close();

    return true;
}

int main ( void )
{


    assert ( flipImage ( "input_00.img", "output_00.img", true, false )
             && identicalFiles ( "output_00.img", "ref_00.img" ) );

    assert ( flipImage ( "input_01.img", "output_01.img", false, true )
             && identicalFiles ( "output_01.img", "ref_01.img" ) );

    assert ( flipImage ( "input_02.img", "output_02.img", true, true )
             && identicalFiles ( "output_02.img", "ref_02.img" ) );

    assert ( flipImage ( "input_03.img", "output_03.img", false, false )
             && identicalFiles ( "output_03.img", "ref_03.img" ) );

    assert ( flipImage ( "input_04.img", "output_04.img", true, false )
             && identicalFiles ( "output_04.img", "ref_04.img" ) );

    assert ( flipImage ( "input_05.img", "output_05.img", true, true )
             && identicalFiles ( "output_05.img", "ref_05.img" ) );

    assert ( flipImage ( "input_06.img", "output_06.img", false, true )
             && identicalFiles ( "output_06.img", "ref_06.img" ) );

    assert ( flipImage ( "input_07.img", "output_07.img", true, false )
             && identicalFiles ( "output_07.img", "ref_07.img" ) );

    assert ( flipImage ( "input_08.img", "output_08.img", true, true )
             && identicalFiles ( "output_08.img", "ref_08.img" ) );

    assert ( ! flipImage ( "input_09.img", "output_09.img", true, false ) );
    // extra inputs (optional & bonus tests)
    /*assert ( flipImage ( "extra_input_00.img", "extra_out_00.img", true, false )
    assert ( flipImage ( "extra_input_01.img", "extra_out_01.img", false, true )
             && identicalFiles ( "extra_out_01.img", "extra_ref_01.img" ) );
    assert ( flipImage ( "extra_input_02.img", "extra_out_02.img", true, false )
             && identicalFiles ( "extra_out_02.img", "extra_ref_02.img" ) );
    assert ( flipImage ( "extra_input_03.img", "extra_out_03.img", false, true )
             && identicalFiles ( "extra_out_03.img", "extra_ref_03.img" ) );
    assert ( flipImage ( "extra_input_04.img", "extra_out_04.img", true, false )
             && identicalFiles ( "extra_out_04.img", "extra_ref_04.img" ) );
    assert ( flipImage ( "extra_input_05.img", "extra_out_05.img", false, true )
             && identicalFiles ( "extra_out_05.img", "extra_ref_05.img" ) );
    assert ( flipImage ( "extra_input_06.img", "extra_out_06.img", true, false )
             && identicalFiles ( "extra_out_06.img", "extra_ref_06.img" ) );
    assert ( flipImage ( "extra_input_07.img", "extra_out_07.img", false, true )
             && identicalFiles ( "extra_out_07.img", "extra_ref_07.img" ) );
    assert ( flipImage ( "extra_input_08.img", "extra_out_08.img", true, false )
             && identicalFiles ( "extra_out_08.img", "extra_ref_08.img" ) );
    assert ( flipImage ( "extra_input_09.img", "extra_out_09.img", false, true )
             && identicalFiles ( "extra_out_09.img", "extra_ref_09.img" ) );
    assert ( flipImage ( "extra_input_10.img", "extra_out_10.img", true, false )
             && identicalFiles ( "extra_out_10.img", "extra_ref_10.img" ) );
    assert ( flipImage ( "extra_input_11.img", "extra_out_11.img", false, true )
             && identicalFiles ( "extra_out_11.img", "extra_ref_11.img" ) );*/
    return 0;
}
#endif /* __PROGTEST__ */

