#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */

struct Land {

    string s_Region;
    unsigned s_ID;

    string s_city;
    string s_address;

    //optional
    string owner;

    Land()
    {
        s_Region = "";
        s_ID = -1;
        s_city = "";
        s_address = "";
        owner = "";
    }

    bool operator > ( const Land & l )
    {
      return (s_city != l.s_city) ? (s_city > l.s_city) :  (s_address > l.s_address) ;
    }

    bool operator < ( const Land & l )
    {
      return (s_city != l.s_city) ? (s_city < l.s_city) :  (s_address < l.s_address) ;
    }


    bool operator >> ( const Land & l )
    {
      return (s_Region != l.s_Region) ? (s_Region > l.s_Region) : (s_ID > l.s_ID);
    }

    bool operator << ( const Land & l )
    {
      return (s_Region != l.s_Region) ? (s_Region < l.s_Region) : (s_ID < l.s_ID);
    }

    bool operator == ( const Land & l )
    {
      return ( s_city == l.s_city ) && ( s_address == l.s_address ) && ( s_Region == l.s_Region ) && ( s_ID == l.s_ID );
    }

};


struct Property {

    string s_owner;

    vector<Land*> owner_properties;

    unsigned size;

    Property()
    {
        s_owner = "";
        size    = 0;
    }

    bool operator > ( const Property & p )
    {
      return this->s_owner > p.s_owner;
    }

    bool operator < ( const Property & p )
    {
      return this->s_owner < p.s_owner;
    }

};

class CIterator
{
  public:


    CIterator( const vector<Land*> * list)
    {
        this->position = (list->size())-1;

        this->a_list = *list;

        this->is_end = ( this->a_list.size() == 0 );

    }

    CIterator()
    {
        this->position = 0;

        this->a_list = vector<Land*>();

        this->is_end = 1;
    }

    bool                     AtEnd                         ( void ) const
    {
        return this->is_end;

    };

    void                     Next                          ( void )
    {
        if(this->position == 0)
        {
            this->is_end = true;
        }

        if( position != 0 )
        {
            position--;
        }

    }

    string                   City                          ( void ) const
    {
        if( this->is_end == 0 )
        {
            return this->a_list[position]->s_city;
        }

        return "";
    };

    string                   Addr                          ( void ) const
    {
        if( this->is_end == 0 )
        {
            return this->a_list[position]->s_address;
        }

        return "";

    };

    string                   Region                        ( void ) const
    {
        if( this->is_end == 0 )
        {
            return this->a_list[position]->s_Region;
        }

        return "";
    };

    unsigned                 ID                            ( void ) const
    {

        if( this->is_end == 0 )
        {
            return this->a_list[position]->s_ID;
        }

        return 0;
    };


    string                   Owner                         ( void ) const
    {
        if( this->is_end == 0 )
        {
            return this->a_list[position]->owner;
        }

        return "";
    };


  private:

    vector<Land*> a_list;

    int position;

    bool is_end;

};

class CLandRegister
{
  public:


    CLandRegister()
    {
      Property* state_property = new Property;

      this->landlords.push_back(state_property);
    }

    ~CLandRegister()
    {

      for( unsigned int i = 0; i < this->lands_by_city_address.size(); i++)
      {
          delete this->lands_by_city_address[i];
      }
//
      this->lands_by_city_address.clear();
      this->lands_by_city_address.shrink_to_fit();


      this->lands_by_region_id.clear();
      this->lands_by_region_id.shrink_to_fit();
//
      for( unsigned int i = 0; i < this->landlords.size(); i++ )
      {
          this->landlords[i]->owner_properties.clear();
          this->landlords[i]->owner_properties.shrink_to_fit();
          delete this->landlords[i];
      }
//
      this->landlords.clear();
      this->landlords.shrink_to_fit();

    }

    bool                     Add                           ( const string    & city,
                                                             const string    & addr,
                                                             const string    & region,
                                                             unsigned int      id );

    bool                     Del                           ( const string    & city,
                                                             const string    & addr );

    bool                     Del                           ( const string    & region,
                                                             unsigned int      id );

    bool                     GetOwner                      ( const string    & city,
                                                             const string    & addr,
                                                             string          & owner ) const;

    bool                     GetOwner                      ( const string    & region,
                                                             unsigned int      id,
                                                             string          & owner ) const;

    bool                     NewOwner                      ( const string    & city,
                                                             const string    & addr,
                                                             const string    & owner );

    bool                     NewOwner                      ( const string    & region,
                                                             unsigned int      id,
                                                             const string    & owner );

    unsigned                 Count                         ( const string    & owner ) const;

    CIterator                ListByAddr                    ( void ) const;

    CIterator                ListByOwner                   ( const string    & owner ) const;

    static void to_lowercase(char& c) {

        c = std::tolower(static_cast<unsigned char>(c));
    }


  private:

    //sorted by city_address for better searching and later for iterator
    vector<Land*>     lands_by_city_address;

    //sorted by region_id for better searching
    vector<Land*>     lands_by_region_id;

    //sorted by owner and date of getting property for better searching
    vector<Property*>  landlords;


    void                  InsertToCityAddress               (Land * new_land);
    void                  InsertToRegionId                  (Land * new_land);
    void                  InsertToLandlords                 (Land * new_land, const string owner);

    void                  DeleteFromCityAddress             (Land * new_land);
    void                  DeleteFromRegionId                (Land * new_land);
    void                  DeleteFromLandlords               (Land * new_land, const string owner);

    int                   GetOwnerPosition                  ( const string &owner, bool toLowercase ) const;
    int                   InsertNewOwner                    ( const string & owner );

    bool                  IsLandRegistred                   (const string & region , unsigned int id);
    bool                  IsLandRegistred                   (const string    & city, const string    & addr);

    Land     *            GetLand                           ( const string    & city,   const    string    & addr ) const;
    Land     *            GetLand                           ( const string    & region, unsigned int         id ) const;

};

// HELPER FUNCTION //

int CLandRegister::InsertNewOwner(const string &owner) {

  Property * new_prop = new Property;

  new_prop->s_owner  = owner;

  for_each(new_prop->s_owner.begin(), new_prop->s_owner.end(), CLandRegister::to_lowercase);

  if( this->landlords.size() == 1 )
  {
      this->landlords.push_back(new_prop);

      return 1;
  }

  for( unsigned int i = 1; i < this->landlords.size(); i++ )
  {
    if (new_prop->s_owner > this->landlords[i]->s_owner) {

      this->landlords.insert(this->landlords.begin() + i, new_prop);

      return i;
    }

    if ((i + 1) == this->landlords.size()) {

      this->landlords.insert(this->landlords.begin() + (i + 1), new_prop);

      return (i + 1);
    }

  }

  return 0;
}

int CLandRegister::GetOwnerPosition(const string &owner, bool toLowercase) const {

  if( owner == "" )
  {
      return 0;
  }

  string b = owner;
  for_each(b.begin(), b.end(), CLandRegister::to_lowercase);

  if( this->landlords.size() == 2 )
  {
      if( this->landlords[0]->s_owner == owner )
      {
          return 0;
      }

      if( this->landlords[1]->s_owner == owner )
      {
          return 1;
      }
  }

  int l = 1;
  int r = (int)(this->landlords.size());

  while ( l <= r && l != (int)(this->landlords.size() ) )
  {
    unsigned int pos = l + ( ( r - l )/2 );

    //cout << pos << endl;

    Property * prop = this->landlords[pos];

    if( prop->s_owner == b )
    {
        //cout << "--------FOUND----------" << endl;
        return pos;
    }

    if( prop->s_owner > b )
    {
      l = pos+1;

    } else {

      r = pos-1;

      }
    }

  //cout << "----------------------" << endl;
  return -1;

}

void CLandRegister::InsertToCityAddress(Land *new_land)
{
    if( this->lands_by_city_address.size() == 0 )
    {
        this->lands_by_city_address.push_back(new_land);

    } else {

      for( unsigned int i = 0; i < this->lands_by_city_address.size(); i++ )
      {
          if( *new_land > *(this->lands_by_city_address[i]) )
          {
            this->lands_by_city_address.insert(this->lands_by_city_address.begin() + i, new_land);

            break;
          }

          if( (i+1) == this->lands_by_city_address.size() )
          {
            this->lands_by_city_address.insert(this->lands_by_city_address.begin() + (i+1), new_land);

            break;
          }


      }
    }
}

void CLandRegister::InsertToRegionId(Land *new_land)
{
  if( this->lands_by_region_id.size() == 0 )
  {
    this->lands_by_region_id.push_back(new_land);

  } else {

    for( unsigned int i = 0; i < this->lands_by_region_id.size(); i++ )
    {

      if( *new_land >> *(this->lands_by_region_id[i]) )
      {
        this->lands_by_region_id.insert(this->lands_by_region_id.begin() + i, new_land);
        break;
      }

      if( (i+1) == this->lands_by_region_id.size() )
      {
        this->lands_by_region_id.insert(this->lands_by_region_id.begin() + (i+1), new_land);
        break;
      }
    }
  }

}

void CLandRegister::InsertToLandlords(Land *new_land, const string owner)
{
    int owner_pos = this->GetOwnerPosition(owner, false);

    if( owner_pos == -1 )
    {
        owner_pos = this->InsertNewOwner(owner);
    }

    if(owner_pos >= 0 && owner_pos <  (int)(this->landlords.size()) )
    {
        this->landlords[owner_pos]->owner_properties.insert(this->landlords[owner_pos]->owner_properties.begin(), new_land);
        this->landlords[owner_pos]->size++;
    }

}


void CLandRegister::DeleteFromCityAddress(Land *new_land)
{
  int l    = 0;
  int r   =  (int)(this->lands_by_city_address.size());

  while( l <= r && l != (int)(this->lands_by_city_address.size()) )
  {
    unsigned int pos = l + (r - l)/2;

    Land * m = this->lands_by_city_address[ pos ];

    if( m->s_city == new_land->s_city && m->s_address == new_land->s_address )
    {
      this->lands_by_city_address.erase(this->lands_by_city_address.begin() + pos);

      break;
    }

    if( *new_land < *(m) )
    {
      l = pos+1;

    } else {

      r = pos - 1;
    }

  }

}

void CLandRegister::DeleteFromRegionId(Land *new_land)
{
  int l    = 0;
  int r   =  (int)(this->lands_by_region_id.size());

  while( l <= r && l != (int)(this->lands_by_region_id.size()) )
  {
    unsigned int pos = l + (r - l)/2;

    if( this->lands_by_region_id[ pos ]->s_Region == new_land->s_Region && this->lands_by_region_id[ pos ]->s_ID == new_land->s_ID )
    {
        this->lands_by_region_id.erase(this->lands_by_region_id.begin() + pos);
        break;
    }

    if( *new_land << *(this->lands_by_region_id[pos])  )
    {
      l = pos+1;

    } else {

      r = pos - 1;
    }

  }

}

void CLandRegister::DeleteFromLandlords(Land *new_land, const string owner)
{

    if( owner == "" )
    {
        //found owner
        for( unsigned int j = 0; j < this->landlords[0]->owner_properties.size(); j++ )
        {
            if(*(this->landlords[0]->owner_properties[j]) == *(new_land))
            {
                this->landlords[0]->owner_properties.erase(this->landlords[0]->owner_properties.begin() + j);
                this->landlords[0]->size--;
                break;
            }

        }
    }

    int l = 1;
    int r = (int)(this->landlords.size());

    string b = owner;
    for_each(b.begin(), b.end(), CLandRegister::to_lowercase);

    while( l <= r && l != (int)(this->landlords.size()) )
      {
        unsigned int pos = l + (r - l)/2;

        if (this->landlords[pos]->s_owner == b) {

            //found owner
            for( unsigned int j = 0; j < this->landlords[pos]->owner_properties.size(); j++ )
              {
                if(*(this->landlords[pos]->owner_properties[j]) == *(new_land))
                {
                    this->landlords[pos]->owner_properties.erase(this->landlords[pos]->owner_properties.begin() + j);
                    this->landlords[pos]->size--;
                    break;
                  }

              }

              break;
          }

          if( b < this->landlords[pos]->s_owner )
          {
            l = pos + 1;

          } else {
            r = pos - 1;
          }

      }

}

Land* CLandRegister::GetLand(const string &region, unsigned int id) const
{

    int l    = 0;
    int r   =  (int)(this->lands_by_region_id.size());

    while( l <= r && l != (int)(this->lands_by_region_id.size()) )
    {
      unsigned int pos = l + (r - l)/2;

      Land * m = this->lands_by_region_id[ pos ];

      if( m->s_Region == region && m->s_ID == id )
      {
        return this->lands_by_region_id[pos];
      }

      if( ( (m->s_Region != region) ? m->s_Region > region : m->s_ID > id ) )
      {
        l = pos+1;

      } else {

        r = pos - 1;
      }

    }

    return nullptr;

}

Land* CLandRegister::GetLand(const string &city, const string &addr) const {

  int l    = 0;
  int r   =  (int)(this->lands_by_city_address.size());

  while( l <= r && l != (int)(this->lands_by_city_address.size()) )
  {
    unsigned int pos = l + (r - l)/2;

    Land * m = this->lands_by_city_address[ pos ];

    if( m->s_city == city && m->s_address == addr )
    {
      return this->lands_by_city_address[pos];
    }

    if( ( (m->s_city != city) ? m->s_city > city : m->s_address > addr ) )
    {
      l = pos+1;

    } else {

      r = pos - 1;
    }

  }

  return nullptr;
}

bool CLandRegister::IsLandRegistred(const string &region, unsigned int id) {

  return  ( this->GetLand(region,id) != nullptr ) ;
}

bool CLandRegister::IsLandRegistred(const string &city, const string &addr) {

  return  ( this->GetLand(city,addr) != nullptr ) ;
}


// BUILD-IN FUNCTION

bool CLandRegister::Add(const string &city, const string &addr, const string &region, unsigned int id)
{

  if( (city.size() == 0 && addr.size() == 0) || (region.size() == 0 && id == 0 ) )
  {
      return false;
  }

  if( this->IsLandRegistred(region,id) || this->IsLandRegistred(city,addr) )
  {
    return false;
  }

  Land * new_land = new Land;

  new_land->s_ID       = id;
  new_land->s_Region   = region;
  new_land->s_address  = addr;
  new_land->s_city     = city;
  new_land->owner      = "";

  this->InsertToCityAddress(new_land);

  this->InsertToRegionId(new_land);

  this->InsertToLandlords(new_land, "");

  return true;

}

bool CLandRegister::Del(const string &region, unsigned int id)
{

    if(!(this->IsLandRegistred(region,id)))
    {
      return false;

    }

    Land * land_to_delete = this->GetLand(region,id);

    this->DeleteFromCityAddress(land_to_delete);
    this->DeleteFromRegionId(land_to_delete);
    this->DeleteFromLandlords(land_to_delete, land_to_delete->owner);

    delete land_to_delete;

    return true;
}

bool CLandRegister::Del(const string &city, const string &addr)
{
  if(!(this->IsLandRegistred(city,addr)))
  {
    return false;
  }

  Land * land_to_delete = this->GetLand(city,addr);

  this->DeleteFromCityAddress(land_to_delete);
  this->DeleteFromRegionId(land_to_delete);
  this->DeleteFromLandlords(land_to_delete, land_to_delete->owner);

  delete  land_to_delete;

  return true;

}

bool CLandRegister::GetOwner(const string &city, const string &addr, string &owner) const
{
    Land * land = this->GetLand(city,addr);

    if( land == nullptr )
    {
      return false;

    }

    owner = land->owner;

    return true;
}


bool CLandRegister::GetOwner(const string &region, unsigned int id, string &owner) const
{

  Land * land = this->GetLand(region,id);

  if( land == nullptr )
  {

    return false;

  }

  owner = land->owner;

  return true;
}


bool CLandRegister::NewOwner(const string &region, unsigned int id, const string &owner)
{

  Land * land = this->GetLand(region,id);

  if( land == nullptr )
  {

    return false;

  }

  if( land->owner == owner ){

      return false;
    }

  this->DeleteFromLandlords(land, land->owner );

  land->owner = owner;

  this->InsertToLandlords(land, owner);

  return true;
}


bool CLandRegister::NewOwner(const string &city, const string &addr, const string &owner)
{

  Land * land = this->GetLand(city,addr);

  if( land == nullptr )
  {

    return false;

  }

  if( land->owner == owner ){

      return false;
  }

  this->DeleteFromLandlords(land, land->owner );

  land->owner = owner;

  this->InsertToLandlords(land, owner);

  return true;
}

unsigned CLandRegister::Count(const string &owner) const
{
    int owner_position = this->GetOwnerPosition(owner, false);

    if( owner_position >= 0 && owner_position < (int)(this->landlords.size()) )
    {
        return (int)(this->landlords[owner_position]->size);
    }

    return 0;

}

CIterator CLandRegister::ListByAddr() const
{
    CIterator land_iterator( &this->lands_by_city_address );

    return land_iterator;
}

CIterator CLandRegister::ListByOwner(const string &owner) const
{
    int owner_pos = this->GetOwnerPosition(owner, true);

    if( owner_pos >= 0 && owner_pos < (int)(this->landlords.size()) )
    {
        CIterator land_iterator( &this->landlords[owner_pos]->owner_properties );

        return land_iterator;

    } else {

        CIterator land_iterator;

        return land_iterator;
    }

}


#ifndef __PROGTEST__
//static void test0 ( void )
//{
//    CLandRegister x;
//    string owner;
//
//    assert ( x . Add ( "Prague", "Thakurova", "Dejvice", 12345 ) );
//    assert ( x . Add ( "Prague", "Evropska", "Vokovice", 12345 ) );
//    assert ( x . Add ( "Prague", "Technicka", "Dejvice", 9873 ) );
//    assert ( x . Add ( "Plzen", "Evropska", "Plzen mesto", 78901 ) );
//    assert ( x . Add ( "Liberec", "Evropska", "Librec", 4552 ) );
//
//    CIterator i0 = x . ListByAddr ();
//    assert ( ! i0 . AtEnd ()
//             && i0 . City () == "Liberec"
//             && i0 . Addr () == "Evropska"
//             && i0 . Region () == "Librec"
//             && i0 . ID () == 4552
//             && i0 . Owner () == "" );
//    i0 . Next ();
//    assert ( ! i0 . AtEnd ()
//             && i0 . City () == "Plzen"
//             && i0 . Addr () == "Evropska"
//             && i0 . Region () == "Plzen mesto"
//             && i0 . ID () == 78901
//             && i0 . Owner () == "" );
//    i0 . Next ();
//    assert ( ! i0 . AtEnd ()
//             && i0 . City () == "Prague"
//             && i0 . Addr () == "Evropska"
//             && i0 . Region () == "Vokovice"
//             && i0 . ID () == 12345
//             && i0 . Owner () == "" );
//    i0 . Next ();
//    assert ( ! i0 . AtEnd ()
//             && i0 . City () == "Prague"
//             && i0 . Addr () == "Technicka"
//             && i0 . Region () == "Dejvice"
//             && i0 . ID () == 9873
//             && i0 . Owner () == "" );
//    i0 . Next ();
//    assert ( ! i0 . AtEnd ()
//             && i0 . City () == "Prague"
//             && i0 . Addr () == "Thakurova"
//             && i0 . Region () == "Dejvice"
//             && i0 . ID () == 12345
//             && i0 . Owner () == "" );
//    i0 . Next ();
//    assert ( i0 . AtEnd () );
//
//    assert ( x . Count ( "" ) == 5 );
//    CIterator i1 = x . ListByOwner ( "" );
//
//    assert ( ! i1 . AtEnd ()
//             && i1 . City () == "Prague"
//             && i1 . Addr () == "Thakurova"
//             && i1 . Region () == "Dejvice"
//             && i1 . ID () == 12345
//             && i1 . Owner () == "" );
//    i1 . Next ();
//
//    assert ( ! i1 . AtEnd ()
//             && i1 . City () == "Prague"
//             && i1 . Addr () == "Evropska"
//             && i1 . Region () == "Vokovice"
//             && i1 . ID () == 12345
//             && i1 . Owner () == "" );
//    i1 . Next ();
//    assert ( ! i1 . AtEnd ()
//             && i1 . City () == "Prague"
//             && i1 . Addr () == "Technicka"
//             && i1 . Region () == "Dejvice"
//             && i1 . ID () == 9873
//             && i1 . Owner () == "" );
//    i1 . Next ();
//    assert ( ! i1 . AtEnd ()
//             && i1 . City () == "Plzen"
//             && i1 . Addr () == "Evropska"
//             && i1 . Region () == "Plzen mesto"
//             && i1 . ID () == 78901
//             && i1 . Owner () == "" );
//    i1 . Next ();
//    assert ( ! i1 . AtEnd ()
//             && i1 . City () == "Liberec"
//             && i1 . Addr () == "Evropska"
//             && i1 . Region () == "Librec"
//             && i1 . ID () == 4552
//             && i1 . Owner () == "" );
//    i1 . Next ();
//    assert ( i1 . AtEnd () );
//
//    //assert ( x . Count ( "CVUT" ) == 0 );
//    CIterator i2 = x . ListByOwner ( "CVUT" );
//    assert ( i2 . AtEnd () );
//
//    assert ( x . NewOwner ( "Prague", "Thakurova", "CVUT" ) );
//    assert ( x . NewOwner ( "Dejvice", 9873, "CVUT" ) );
//    assert ( x . NewOwner ( "Plzen", "Evropska", "Anton Hrabis" ) );
//    assert ( x . NewOwner ( "Librec", 4552, "Cvut" ) );
//    assert ( x . GetOwner ( "Prague", "Thakurova", owner ) && owner == "CVUT" );
//    assert ( x . GetOwner ( "Dejvice", 12345, owner ) && owner == "CVUT" );
//    assert ( x . GetOwner ( "Prague", "Evropska", owner ) && owner == "" );
//    assert ( x . GetOwner ( "Vokovice", 12345, owner ) && owner == "" );
//    assert ( x . GetOwner ( "Prague", "Technicka", owner ) && owner == "CVUT" );
//    assert ( x . GetOwner ( "Dejvice", 9873, owner ) && owner == "CVUT" );
//    assert ( x . GetOwner ( "Plzen", "Evropska", owner ) && owner == "Anton Hrabis" );
//    assert ( x . GetOwner ( "Plzen mesto", 78901, owner ) && owner == "Anton Hrabis" );
//    assert ( x . GetOwner ( "Liberec", "Evropska", owner ) && owner == "Cvut" );
//    assert ( x . GetOwner ( "Librec", 4552, owner ) && owner == "Cvut" );
//    CIterator i3 = x . ListByAddr ();
//    assert ( ! i3 . AtEnd ()
//             && i3 . City () == "Liberec"
//             && i3 . Addr () == "Evropska"
//             && i3 . Region () == "Librec"
//             && i3 . ID () == 4552
//             && i3 . Owner () == "Cvut" );
//    i3 . Next ();
//    assert ( ! i3 . AtEnd ()
//             && i3 . City () == "Plzen"
//             && i3 . Addr () == "Evropska"
//             && i3 . Region () == "Plzen mesto"
//             && i3 . ID () == 78901
//             && i3 . Owner () == "Anton Hrabis" );
//    i3 . Next ();
//    assert ( ! i3 . AtEnd ()
//             && i3 . City () == "Prague"
//             && i3 . Addr () == "Evropska"
//             && i3 . Region () == "Vokovice"
//             && i3 . ID () == 12345
//             && i3 . Owner () == "" );
//    i3 . Next ();
//    assert ( ! i3 . AtEnd ()
//             && i3 . City () == "Prague"
//             && i3 . Addr () == "Technicka"
//             && i3 . Region () == "Dejvice"
//             && i3 . ID () == 9873
//             && i3 . Owner () == "CVUT" );
//    i3 . Next ();
//    assert ( ! i3 . AtEnd ()
//             && i3 . City () == "Prague"
//             && i3 . Addr () == "Thakurova"
//             && i3 . Region () == "Dejvice"
//             && i3 . ID () == 12345
//             && i3 . Owner () == "CVUT" );
//    i3 . Next ();
//    assert ( i3 . AtEnd () );
//
//    //assert ( x . Count ( "cvut" ) == 3 );
//    CIterator i4 = x . ListByOwner ( "cVuT" );
//    assert ( ! i4 . AtEnd ()
//             && i4 . City () == "Prague"
//             && i4 . Addr () == "Thakurova"
//             && i4 . Region () == "Dejvice"
//             && i4 . ID () == 12345
//             && i4 . Owner () == "CVUT" );
//    i4 . Next ();
//    assert ( ! i4 . AtEnd ()
//             && i4 . City () == "Prague"
//             && i4 . Addr () == "Technicka"
//             && i4 . Region () == "Dejvice"
//             && i4 . ID () == 9873
//             && i4 . Owner () == "CVUT" );
//    i4 . Next ();
//    assert ( ! i4 . AtEnd ()
//             && i4 . City () == "Liberec"
//             && i4 . Addr () == "Evropska"
//             && i4 . Region () == "Librec"
//             && i4 . ID () == 4552
//             && i4 . Owner () == "Cvut" );
//    i4 . Next ();
//    assert ( i4 . AtEnd () );
//
//    assert ( x . NewOwner ( "Plzen mesto", 78901, "CVut" ) );
//
//    assert ( x . Count ( "CVUT" ) == 4 );
//
//    CIterator i5 = x . ListByOwner ( "CVUT" );
//    assert ( ! i5 . AtEnd ()
//             && i5 . City () == "Prague"
//             && i5 . Addr () == "Thakurova"
//             && i5 . Region () == "Dejvice"
//             && i5 . ID () == 12345
//             && i5 . Owner () == "CVUT" );
//    i5 . Next ();
//    assert ( ! i5 . AtEnd ()
//             && i5 . City () == "Prague"
//             && i5 . Addr () == "Technicka"
//             && i5 . Region () == "Dejvice"
//             && i5 . ID () == 9873
//             && i5 . Owner () == "CVUT" );
//    i5 . Next ();
//    assert ( ! i5 . AtEnd ()
//             && i5 . City () == "Liberec"
//             && i5 . Addr () == "Evropska"
//             && i5 . Region () == "Librec"
//             && i5 . ID () == 4552
//             && i5 . Owner () == "Cvut" );
//    i5 . Next ();
//    assert ( ! i5 . AtEnd ()
//             && i5 . City () == "Plzen"
//             && i5 . Addr () == "Evropska"
//             && i5 . Region () == "Plzen mesto"
//             && i5 . ID () == 78901
//             && i5 . Owner () == "CVut" );
//    i5 . Next ();
//    assert ( i5 . AtEnd () );
//
//    assert ( x . Del ( "Liberec", "Evropska" ) );
//    assert ( x . Del ( "Plzen mesto", 78901 ) );
//    assert ( x . Count ( "cvut" ) == 2 );
//    CIterator i6 = x . ListByOwner ( "cVuT" );
//    assert ( ! i6 . AtEnd ()
//             && i6 . City () == "Prague"
//             && i6 . Addr () == "Thakurova"
//             && i6 . Region () == "Dejvice"
//             && i6 . ID () == 12345
//             && i6 . Owner () == "CVUT" );
//    i6 . Next ();
//    assert ( ! i6 . AtEnd ()
//             && i6 . City () == "Prague"
//             && i6 . Addr () == "Technicka"
//             && i6 . Region () == "Dejvice"
//             && i6 . ID () == 9873
//             && i6 . Owner () == "CVUT" );
//    i6 . Next ();
//    assert ( i6 . AtEnd () );
//
//    assert ( x . Add ( "Liberec", "Evropska", "Librec", 4552 ) );
//}
//
//
//static void test1 ( void )
//{
//    CLandRegister x;
//    string owner;
//
//    assert ( x . Add ( "Prague", "Thakurova", "Dejvice", 12345 ) );
//    assert ( x . Add ( "Prague", "Evropska", "Vokovice", 12345 ) );
//    assert ( x . Add ( "Prague", "Technicka", "Dejvice", 9873 ) );
//    assert ( ! x . Add ( "Prague", "Technicka", "Hradcany", 7344 ) );
//    assert ( ! x . Add ( "Brno", "Bozetechova", "Dejvice", 9873 ) );
//
//    assert ( !x . GetOwner ( "Prague", "THAKUROVA", owner ) );
//    assert ( !x . GetOwner ( "Hradcany", 7343, owner ) );
//
//    CIterator i0 = x . ListByAddr ();
//    assert ( ! i0 . AtEnd ()
//             && i0 . City () == "Prague"
//             && i0 . Addr () == "Evropska"
//
//             && i0 . Region () == "Vokovice"
//             && i0 . ID () == 12345
//             && i0 . Owner () == "" );
//
//    i0 . Next ();
//
//
//    assert ( ! i0 . AtEnd ()
//             && i0 . City () == "Prague"
//             && i0 . Addr () == "Technicka"
//             && i0 . Region () == "Dejvice"
//             && i0 . ID () == 9873
//             && i0 . Owner () == "" );
//    i0 . Next ();
//
//    assert ( ! i0 . AtEnd ()
//             && i0 . City () == "Prague"
//             && i0 . Addr () == "Thakurova"
//             && i0 . Region () == "Dejvice"
//             && i0 . ID () == 12345
//             && i0 . Owner () == "" );
//    i0 . Next ();
//    assert ( i0 . AtEnd () );
//
//    assert ( x . NewOwner ( "Prague", "Thakurova", "CVUT" ) );
//    assert ( ! x . NewOwner ( "Prague", "technicka", "CVUT" ) );
//    assert ( ! x . NewOwner ( "prague", "Technicka", "CVUT" ) );
//    assert ( ! x . NewOwner ( "dejvice", 9873, "CVUT" ) );
//    assert ( ! x . NewOwner ( "Dejvice", 9973, "CVUT" ) );
//    assert ( ! x . NewOwner ( "Dejvice", 12345, "CVUT" ) );
//
//    assert ( x . Count ( "CVUT" ) == 1 );
//
//    CIterator i1 = x . ListByOwner ( "CVUT" );
//    assert ( ! i1 . AtEnd ()
//             && i1 . City () == "Prague"
//             && i1 . Addr () == "Thakurova"
//             && i1 . Region () == "Dejvice"
//             && i1 . ID () == 12345
//             && i1 . Owner () == "CVUT" );
//    i1 . Next ();
//
//    assert ( i1 . AtEnd () );
//
//    assert ( ! x . Del ( "Brno", "Technicka" ) );
//    assert ( ! x . Del ( "Karlin", 9873 ) );
//    assert ( x . Del ( "Prague", "Technicka" ) );
//    assert ( ! x . Del ( "Prague", "Technicka" ) );
//    assert ( ! x . Del ( "Dejvice", 9873 ) );
//}

int main ( void )
{
    //test0();
    //test1();

    CLandRegister x;
    string owner;

    assert ( x . Add ( "Prague", "Thakurova", "Dejvice", 12345 ) );
    assert ( x . Add ( "Prague", "Evropska", "Vokovice", 12345 ) );
    assert ( x . Add ( "Prague", "Technicka", "Dejvice", 9873 ) );
    assert ( ! x . Add ( "Prague", "Technicka", "Hradcany", 7344 ) );
    assert ( ! x . Add ( "Brno", "Bozetechova", "Dejvice", 9873 ) );

    assert ( x . Add ( "AS", "FD", "ASD", 3421 ) );
    assert ( x . Add ( "asfdas", "afasdfas", "asdf", 43543 ) );
    assert ( x . Add ( "asdfgt", "fdsA", "HGJHGD", 44 ) );
    assert ( x . Add ( "44", "AA", "FDDSSA", 5452654 ) );
    assert ( x . Add ( "FASDFASG", "GAFDGADF", "ASDFD", 43 ) );

    x.NewOwner("Prague", "Thakurova", "sadfadfgadf");
    x.NewOwner("Vokovice", 12345, "asfdasdfasf");
    x.NewOwner("Prague", "Technicka", "fddfffds");
    x.NewOwner("Prague", "Technicka", "jhdjfsfaga");

    cout << x.Count("sadfadfgadf") << endl;
    cout << x.Count("asfdasdfasf") << endl;
    cout << x.Count("fddfffds") << endl;
    cout << x.Count("jhdjfsfaga") << endl;
    cout << x.Count("AAAAA") << endl;

    return 0;
}
#endif /* __PROGTEST__ */
