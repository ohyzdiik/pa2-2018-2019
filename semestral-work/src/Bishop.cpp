#include "Bishop.hpp"

//======================================================================================================================
Bishop::Bishop(const unsigned &x, const unsigned &y, const COLOR & color) {

    this->x = x;
    this->y = y;
    this->name = "BISHOP";
    this->alive = true;
    this->c = color;
}
//======================================================================================================================
Bishop::Bishop() {

    this->name = "BISHOP";
    this->alive = true;
}
//======================================================================================================================
Bishop::Bishop(const Bishop &b) {

    this->x  = b.x;
    this->y  = b.y;
    this->name = "BISHOP";
    this->alive = b.alive;
    this->c = b.c;
    this->last_position = b.last_position;
}
//======================================================================================================================
Bishop::Bishop(const COLOR &c, const unsigned &x, const unsigned &y, const unsigned &last_x, const unsigned &last_y,
               const bool &alive) {

    this->x  = x;
    this->y  = y;
    this->name = "BISHOP";
    this->alive = alive;
    this->c = c;
    this->last_position = std::make_pair(last_x, last_y);
}
//======================================================================================================================
Bishop& Bishop::operator=(const Bishop &b) {

    this->x  = b.x;
    this->y  = b.y;
    this->name = "BISHOP";
    this->alive = b.alive;
    this->c = b.c;
    this->last_position = b.last_position;

    return *this;
}
//======================================================================================================================
bool Bishop::setPosition(const unsigned &x, const unsigned &y) {

    if( x > 8 || x < 1 || y > 8 || y < 1 )
        return 0;

    if ( this->x == x || this->y == y )
        return 0;

    this->last_position = std::make_pair(this->x, this->y);
    this->x = x;
    this->y = y;

    return 1;
}
//======================================================================================================================
bool Bishop::threatenPosition(unsigned x, unsigned y) {

    unsigned y_position_upper = y;
    unsigned y_position_lower = y;

    for( unsigned x_position = x; x_position <= 8; x_position++ )
    {
        if( x_position == this->x && (y_position_upper == this->y || y_position_lower == this->y ) )
            return true;

        y_position_upper++;
        y_position_lower--;
    }

    y_position_upper = y;
    y_position_lower = y;

    for( unsigned x_position = x; x_position >= 1; x_position-- )
    {
        if( x_position == this->x && (y_position_upper == this->y || y_position_lower == this->y ) )
            return true;

        y_position_upper++;
        y_position_lower--;

    }

    if ( this->x == x || this->y == y )
        return true;

    return false;
}
//======================================================================================================================
std::set<std::pair<std::string , unsigned >> Bishop::calculateAllPossibleMoves  ()
{
    std::set<std::pair<std::string, unsigned >> all_moves;

    unsigned left_x      = this->x+1;
    unsigned right_x     = this->x-1;

    for( unsigned r_y = this->y+1; r_y <= 8; r_y++ )
    {
        if ( (left_x) >= 1 && (left_x) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(left_x), r_y));

        if ( (right_x) >= 1 && (right_x) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(right_x), r_y));

        left_x++;
        right_x++;
    }

    left_x      = this->x+1;
    right_x     = this->x-1;

    for( unsigned r_y = this->y-1; r_y >= 1; r_y-- )
    {
        if ( (left_x) >= 1 && (left_x) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(left_x), r_y));

        if ( (right_x) >= 1 && (right_x) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(right_x), r_y));

        left_x++;
        right_x++;
    }

    return all_moves;
}
//======================================================================================================================
Bishop* Bishop::Copy() {
    return new Bishop(*this);
}
//==================================================================================================================