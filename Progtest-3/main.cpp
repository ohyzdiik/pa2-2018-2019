#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
using namespace std;
class InvalidRangeException
{
};
#endif /* __PROGTEST__ */

// uncomment if your code implements initializer lists
// #define EXTENDED_SYNTAX

class CRange
{
public:

    //=================================================================================================

    CRange                  ()
    {
        this->lo = 0;
        this->lo = 0;
    };

    //-------------------------------------------------------------------------------------------------

    CRange                  ( const long long lo, const long long hi )
    {
        if( (lo > hi)  )
        {
            throw InvalidRangeException();
        }

        this->lo = lo;
        this->hi = hi;

    }

    //-------------------------------------------------------------------------------------------------

    CRange                  ( const CRange & x )
    {
        this->hi = x.hi;
        this->lo = x.lo;
    }

    //-------------------------------------------------------------------------------------------------

    const bool               liesWithin                 ( const long long & x ) const
    {
        return (this->lo <= x) && (x <= this->hi);
    }

    //-------------------------------------------------------------------------------------------------

    const bool                liesWithin                 ( const CRange & x ) const
    {
        return ( ( this->lo <= x.lo ) && ( x.hi <= this->hi ) );
    }
    //-------------------------------------------------------------------------------------------------

    const bool                overlies                    (const CRange & x ) const
    {
        return (( this->lo <= x.hi && x.hi <= this->hi ) || ( this->lo <= x.lo && x.lo <= this->hi ));
    }

    //-------------------------------------------------------------------------------------------------

    bool                consecutive                     ( const CRange & x ) const
    {

        return ( (this->lo+1 == x.lo && x.lo != LLONG_MIN && this->hi != LLONG_MAX ) || ( x.lo + 1 == this->hi && x.lo != LLONG_MIN && x.hi != LLONG_MAX ) ) || ( (this->hi + 1) == (x.lo)  && x.lo != LLONG_MIN && this->hi != LLONG_MAX) || ( (x.hi + 1) == (this->lo)  && x.lo != LLONG_MIN && x.hi != LLONG_MAX );
    }

    //-------------------------------------------------------------------------------------------------

    friend bool         operator<                   (const CRange & x1, const CRange & x2 )
    {

        return (x1.lo < x2.lo) && (x1.hi < x2.hi);
    }
    //-------------------------------------------------------------------------------------------------

    friend bool         operator>                   (const CRange & x1, const CRange & x2 )
    {

        return (x1.lo > x2.lo) && (x1.hi > x2.hi);
    }

    //-------------------------------------------------------------------------------------------------

    friend bool         operator<                   (const CRange & x1, const long long & x2 )
    {

        return (x1.lo < x2) && (x1.hi < x2);
    }
    //-------------------------------------------------------------------------------------------------

    friend bool         operator>                   (const CRange & x1, const long long & x2  )
    {

        return (x1.lo > x2) && (x1.hi > x2);
    }


    //-------------------------------------------------------------------------------------------------

    friend bool         operator==                  (const CRange &x1, const CRange & x2 )
    {
        return (x1.lo == x2.lo) && (x1.hi == x2.hi);
    }

    //-------------------------------------------------------------------------------------------------

    friend bool         operator!=                  (const CRange &x1, const CRange & x2 )
    {
        return !((x1.lo == x2.lo) && (x1.hi == x2.hi));
    }


    //-------------------------------------------------------------------------------------------------

    string              toString                 () const
    {

        if(this->lo != this->hi)
        {
            return "<"+to_string(this->lo) + ".." + to_string(this->hi) + ">";
        }

        return to_string(this->hi);

    }

    long long hi, lo;
};

//-------- MAGIC OPERATOR SOLVER -------------------------------------------------------------------------

enum OP_TYPE {

    ADD,
    SUBSTRACT
};

struct RangeOperations {

    long lo, hi;
    OP_TYPE type;

};


const vector<RangeOperations> operation_list( OP_TYPE optype1 , long long lo1, long long hi1, OP_TYPE optype2, long long lo2, long long hi2 )
{
    vector<RangeOperations> operation_list;
    RangeOperations op1, op2;

    op1.hi          = hi1;
    op1.lo          = lo1;
    op1.type        = optype1;

    op2.hi          = hi2;
    op2.lo          = lo2;
    op2.type        = optype2;

    operation_list.push_back(op1);
    operation_list.push_back(op2);

    return operation_list;
}

const vector<RangeOperations>       operator+           (const CRange & a, const CRange & b )
{

    return operation_list(ADD, a.lo, a.hi, ADD, b.lo, b.hi);

}

const vector<RangeOperations>       operator-           (const CRange & a, const CRange & b )
{
    return operation_list(ADD, a.lo, a.hi, SUBSTRACT, b.lo, b.hi);
}

const vector<RangeOperations>       operator-           (const vector<RangeOperations> &a ,const CRange & b)
{
    vector<RangeOperations> operation_list;

    for( int i = 0; i < (int)(a.size()); i++ )
    {
        operation_list.push_back(a[i]);
    }

    RangeOperations op;

    op.hi = b.hi;
    op.lo = b.lo;
    op.type = SUBSTRACT;

    operation_list.push_back(op);

    return operation_list;
}

const vector<RangeOperations>       operator+           (const vector<RangeOperations> &a ,const CRange & b)
{
    vector<RangeOperations> operation_list;

    for( int i = 0; i < (int)(a.size()); i++ )
    {
        operation_list.push_back(a[i]);
    }

    RangeOperations op;

    op.hi = b.hi;
    op.lo = b.lo;
    op.type = ADD;

    operation_list.push_back(op);

    return operation_list;
}


//-------- END -------------------------------------------------------------------------


class CRangeList
{
public:

    //=================================================================================================

    CRangeList              () {};

    //-------------------------------------------------------------------------------------------------

    ~CRangeList             ()
    {
        this->range_list.clear();
    };

    //-------------------------------------------------------------------------------------------------

    CRangeList              ( const CRangeList & x )
    {
        for( int i = 0; i < (int)(x.range_list.size()); i++  )
        {
            this->range_list.push_back(x.range_list[i]);
        }
    };

    //-------------------------------------------------------------------------------------------------

    CRangeList              ( const CRange & x )
    {
        this->range_list.push_back(x);
    }

    //-------------------------------------------------------------------------------------------------

    bool                    Includes                (  const long long & x) const

    {

        int l = 1;

        int r = (int)(this->range_list.size());

        while( l<= r && l != (int)(( this->range_list.size())) )
        {
            unsigned int pos = l + ( ( r - l )/2 );

            if( this->range_list[pos].liesWithin(x) )
            {
                return true;
            }

            if( this->range_list[pos] < x )
            {
                l = pos + 1;

            } else {

                r = pos - 1;
            }

        }

        return false;

    };

    //-------------------------------------------------------------------------------------------------

    bool                    Includes                (  const CRange & x) const
    {
        int l = 1;

        int r = (int)(this->range_list.size());

        while( l<= r && l != (int)(( this->range_list.size())) )
        {
            unsigned int pos = l + ( ( r - l )/2 );

            if( this->range_list[pos].liesWithin(x) )
            {
                return true;
            }

            if( this->range_list[pos] < x )
            {
                l = pos + 1;

            } else {

                r = pos - 1;
            }

        }

        return false;

    };

    //-------------------------------------------------------------------------------------------------

    bool            operator+=                  (const CRange & x)
    {
        if( (int)(this->range_list.size()) == 0 )
        {
            this->range_list.push_back(x);

            return true;

        } else {

            for( int i = 0; i < (int)(this->range_list.size()); i++ )
            {
                // ---------------------------------------

                if( this->range_list[i].liesWithin(x) )
                {

                    return true;
                }

                // ---------------------------------------

                if( x.liesWithin(this->range_list[i])  || this->range_list[i].consecutive(x) || this->range_list[i].overlies(x) )
                {

                    this->fixByAdd(i,x);

                    return true;
                }

                // ---------------------------------------

                if ( x < this->range_list[i] )
                {
                    this->range_list.insert(this->range_list.begin()+i,  x);

                    break;
                }

                // ---------------------------------------

                if( (i+1) == (int)(this->range_list.size()) )
                {
                    this->range_list.insert(this->range_list.begin()+i+1,  x);

                    break;
                }

                // ---------------------------------------
            }

        }


        return true;

    }

    //-------------------------------------------------------------------------------------------------

    bool            operator+=                  (const vector<RangeOperations> & x)
    {
        CRangeList tmp;

        for( int i = 0; i < (int)(x.size()); i++ )
        {
            RangeOperations op = x[i];

            CRange a(op.lo, op.hi);

            if( op.type == ADD )
            {
                tmp += a;

            } else {

                tmp -= a;
            }
        }

        (*this) += tmp;

        return true;
    }

    //-------------------------------------------------------------------------------------------------

    bool                    operator+=              ( const CRangeList x )
    {
        for( int j = 0; j < (int)(x.range_list.size()); j++ )
        {
            *this += x.range_list[j];
        }

        return true;
    };

    //-------------------------------------------------------------------------------------------------

    bool            operator-=                  (const CRange & x)
    {
        if( (int)(this->range_list.size()) == 0 || ( x.liesWithin( this->range_list.front() )  && x.liesWithin( this->range_list.back() ) ) )
        {
            this->range_list.clear();

            return true;

        } else {

            for( int i = 0; i < (int)(this->range_list.size()); i++ )
            {

                if( x == this->range_list[i])
                {

                    this->range_list.erase(this->range_list.begin() + i);
                    i--;
                    break;
                }

                if( x.liesWithin(this->range_list[i]) )
                {
                    for(int j = i; j < (int)( this->range_list.size() ) ;  j++ )
                    {
                        if( x.liesWithin(this->range_list[j]) )
                        {
                            this->range_list.erase(this->range_list.begin()+j);

                            j--;

                        } else {

                            this->range_list[j].lo = ( this->range_list[j].lo < x.hi) ? (x.hi+1) : this->range_list[j].lo ;

                            break;
                        }
                    }


                    return true;
                }

                if( this->range_list[i].overlies(x) )
                {

                    long long lo_old =  this->range_list[i].lo;
                    long long hi_old =  this->range_list[i].hi;


                    this->range_list[i].lo  = ( x.lo == this->range_list[i].lo || x.lo < this->range_list[i].lo ) ? ((x.hi != LLONG_MAX) ? x.hi + 1 : x.hi ) : this->range_list[i].lo;

                    this->range_list[i].hi  = ( x.hi == this->range_list[i].hi )
                                              ?  ((x.lo != LLONG_MIN) ? x.lo-1 : x.lo)
                                              : ( ( x.hi < this->range_list[i].hi && this->range_list[i].lo < x.hi)
                                                  ? ((x.lo != LLONG_MIN) ? x.lo-1 : x.lo)
                                                  :
                                                  (x.hi > hi_old)
                                                  ? ( (x.lo != LLONG_MIN) ? x.lo-1 : x.lo )
                                                  : this->range_list[i].hi );


                    CRange new_stuff ( ((x.hi != LLONG_MAX) ? x.hi + 1 : x.hi ), ((x.hi != LLONG_MAX) ? x.hi + 12: x.hi ) );

                    for( int j = i+1 ; j < (int)(this->range_list.size()) ; j++ )
                    {
                        if( x.liesWithin(this->range_list[j]) )
                        {
                            this->range_list.erase(this->range_list.begin() + j);

                            j--;

                            continue;
                        }

                        if( this->range_list[j].overlies(x) )
                        {

                            new_stuff.hi = this->range_list[j].hi;

                            this->range_list.erase(this->range_list.begin() + j);

                            if( new_stuff < this->range_list[i] )
                            {
                                this->range_list.insert(this->range_list.begin() + i, new_stuff);

                            } else {

                                this->range_list.insert(this->range_list.begin() + i+1, new_stuff);
                            }

                            return true;
                        }
                    }

                    if( lo_old < x.lo && x.hi < hi_old )
                    {

                        new_stuff.lo = ( (x.hi != LLONG_MAX) ? x.hi + 1 : x.hi );
                        new_stuff.hi = hi_old;

                        this->range_list.insert(this->range_list.begin() + i+1, new_stuff);
                    }


                    return true;

                }
            }

            return true;
        }

    }

    //-------------------------------------------------------------------------------------------------

    bool            operator-=                  ( const vector<RangeOperations> & x )
    {
        CRangeList tmp;

        for( int i = 0; i < (int)(x.size()); i++ )
        {
            RangeOperations op = x[i];

            CRange a(op.lo, op.hi);

            if( op.type == SUBSTRACT )
            {
                tmp -= a;

            } else {

                tmp += a;
            }
        }


        (*this) -= tmp;

        return true;
    }

    //-------------------------------------------------------------------------------------------------

    bool                    operator-=              ( const CRangeList x )
    {
        for( int j = 0; j < (int)(x.range_list.size()); j++ )
        {
            *this -= x.range_list[j];
        }

        return true;
    };


    //-------------------------------------------------------------------------------------------------

    CRangeList &               operator=               (const CRangeList & x )
    {
        this->range_list.clear();

        for( int i = 0; i < (int)(x.range_list.size()); i++ )
        {
            this->range_list.push_back( x.range_list[i] );
        }

        return *this;
    }

    //-------------------------------------------------------------------------------------------------

    CRangeList &               operator=               (const CRange & x )
    {
        this->range_list.clear();

        this->range_list.push_back(x);

        return *this;
    }

    //-------------------------------------------------------------------------------------------------

    CRangeList &               operator=               ( const vector<RangeOperations> & x )
    {
        this->range_list.clear();

        for( int i = 0; i < (int)(x.size()); i++ )
        {
            RangeOperations op = x[i];

            CRange a(op.lo, op.hi);

            if( op.type == ADD )
            {
                *(this) += a;

            } else {

                *(this) -= a;
            }
        }

        return *this;
    }

    //-------------------------------------------------------------------------------------------------

    friend bool                  operator==             (const CRangeList & x1, const CRangeList & x2)
    {

        return equal(x1.range_list.begin(), x1.range_list.end(), x2.range_list.begin());

    }

    //-------------------------------------------------------------------------------------------------

    friend bool                  operator!=             (const CRangeList & x1, const CRangeList & x2)
    {

        return !(equal(x1.range_list.begin(), x1.range_list.end(), x2.range_list.begin()));

    }

    //-------------------------------------------------------------------------------------------------

    friend std::ostream&        operator<<             ( std::ostream & stream ,  const CRangeList & x )
    {

        stream << "{";

        for( int i = 0 ; i < (int)(x.range_list.size()); i++ )
        {
            stream << x.range_list[i].toString();

            if(i+1 < (int)(x.range_list.size()))
            {
                stream << ",";
            }
        }

        stream << "}";

        return stream;
    }

    //=================================================================================================

private:

    vector<CRange> range_list;

    bool fixByAdd ( const int i , const CRange & x )
    {
        this->range_list[i].lo = ( this->range_list[i].lo  < x.lo ) ? this->range_list[i].lo : x.lo;

        this->range_list[i].hi = ( this->range_list[i].hi  < x.hi ) ? x.hi : this->range_list[i].hi;

        for(int j = i+1; j < (int)(this->range_list.size()); j++ )
        {
            if( x.liesWithin(this->range_list[j]) || x.consecutive(this->range_list[j]) || x.overlies(this->range_list[j]) )
            {
                this->range_list[i].hi = this->range_list[j].hi;

                this->range_list.erase(this->range_list.begin() + j);

                j--;

            }  else {

                break;
            }
        }

        this->range_list[i].hi = (x.hi > this->range_list[i].hi) ? x.hi : this->range_list[i].hi;

        return true;
    }
};

#ifndef __PROGTEST__
string             toString                                ( const CRangeList& x )
{
    ostringstream oss;
    oss << x;
    return oss . str ();
}

int                main                                    ( void )
{
    CRangeList a, b;

    assert ( sizeof ( CRange ) <= 2 * sizeof ( long long ) );
    a = CRange ( 5, 10 );
    a += CRange ( 25, 100 );
    assert ( toString ( a ) == "{<5..10>,<25..100>}" );
    a += CRange ( -5, 0 );
    a += CRange ( 8, 50 );
    assert ( toString ( a ) == "{<-5..0>,<5..100>}" );
    a += CRange ( 101, 105 ) + CRange ( 120, 150 )  + CRange ( 160, 180 ) + CRange ( 190, 210 );

    assert ( toString ( a ) == "{<-5..0>,<5..105>,<120..150>,<160..180>,<190..210>}" );
    a += CRange ( 106, 119 ) + CRange ( 152, 158 );
    assert ( toString ( a ) == "{<-5..0>,<5..150>,<152..158>,<160..180>,<190..210>}" );
    a += CRange ( -3, 170 );
    a += CRange ( -30, 1000 );
    assert ( toString ( a ) == "{<-30..1000>}" );
    b = CRange ( -500, -300 ) + CRange ( 2000, 3000 ) ;
    a += b;
    a +=  CRange ( 700, 1001 );
    assert ( toString ( a ) == "{<-500..-300>,<-30..1001>,<2000..3000>}" );
    a -= CRange ( -400, -400 );
    assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..1001>,<2000..3000>}" );
    a -= CRange ( 10, 20 ) +  CRange ( 900, 2500 ) + CRange ( 30, 40 ) + CRange ( 10000, 20000 );
    assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
    try
    {
        a += CRange ( 15, 18 ) + CRange ( 10, 0 ) + CRange ( 35, 38 );
        assert ( "Exception not thrown" == NULL );
    }
    catch ( const InvalidRangeException & e )
    {
    }
    catch ( ... )
    {
        assert ( "Invalid exception thrown" == NULL );
    }
    assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
    b = a;
    assert ( a == b );
    assert ( !( a != b ) );
    b += CRange ( 2600, 2700 );

    assert ( toString ( b ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
    assert ( a == b );
    assert ( !( a != b ) );
    b += CRange ( 15, 15 );
    assert ( toString ( b ) == "{<-500..-401>,<-399..-300>,<-30..9>,15,<21..29>,<41..899>,<2501..3000>}" );
    assert ( !( a == b ) );
    assert ( a != b );
    assert ( b . Includes ( 15 ) );
    assert ( b . Includes ( 2900 ) );
    assert ( b . Includes ( CRange ( 15, 15 ) ) );
    assert ( b . Includes ( CRange ( -350, -350 ) ) );
    assert ( b . Includes ( CRange ( 100, 200 ) ) );
    assert ( !b . Includes ( CRange ( 800, 900 ) ) );
    assert ( !b . Includes ( CRange ( -1000, -450 ) ) );
    assert ( !b . Includes ( CRange ( 0, 500 ) ) );
    a += CRange ( -10000, 10000 ) + CRange ( 10000000, 1000000000 );
    assert ( toString ( a ) == "{<-10000..10000>,<10000000..1000000000>}" );
    b += a;
    assert ( toString ( b ) == "{<-10000..10000>,<10000000..1000000000>}" );
    b -= a;
    assert ( toString ( b ) == "{}" );
    b += CRange ( 0, 100 ) + CRange ( 200, 300 ) - CRange ( 150, 250 ) + CRange ( 160, 180 ) - CRange ( 170, 170 );
    assert ( toString ( b ) == "{<0..100>,<160..169>,<171..180>,<251..300>}" );
    b -= CRange ( 10, 90 ) - CRange ( 20, 30 ) - CRange ( 40, 50 ) - CRange ( 60, 90 ) + CRange ( 70, 80 );
    assert ( toString ( b ) == "{<0..9>,<20..30>,<40..50>,<60..69>,<81..100>,<160..169>,<171..180>,<251..300>}" );

    CRangeList c,d;

    c+=CRange(LLONG_MIN, 12);
    d+=CRange(10, LLONG_MAX);

    cout << d << endl;
    cout << c << endl;

    c+=d;

    cout << c << endl;

#ifdef EXTENDED_SYNTAX
    CRangeList x { { 5, 20 }, { 150, 200 }, { -9, 12 }, { 48, 93 } };
  assert ( toString ( x ) == "{<-9..20>,<48..93>,<150..200>}" );
  ostringstream oss;
  oss << setfill ( '=' ) << hex << left;
  for ( const auto & v : x + CRange ( -100, -100 ) )
    oss << v << endl;
  oss << setw ( 10 ) << 1024;
  assert ( oss . str () == "-100\n<-9..20>\n<48..93>\n<150..200>\n400=======" );
#endif /* EXTENDED_SYNTAX */
    return 0;
}
#endif /* __PROGTEST__ */
