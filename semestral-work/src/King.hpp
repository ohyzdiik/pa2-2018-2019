#pragma once

#include "Figure.hpp"

class King : public Figure
{
public:
    //==================================================================================================================
    /**
    * Constructor for initialization of chessboard
    * @param x - starting X position
    * @param y - starting Y position
    * @param color - color of player
    */
    King                                                                        (   const unsigned & x,
                                                                                    const unsigned & y,
                                                                                    const COLOR & color );
    //==================================================================================================================
    /**
    * Copy constructor
    * @param k - object from copy
    */
    King                                                                        ( const King & k );
    //==================================================================================================================
    /**
     * Empty constructor
     */
    King                                                                        ();
    //==================================================================================================================
    /**
    * Overloaded operator =
    * @param k - object to copy from
    * @return - copied object
    */
    King& operator=                                                             ( const King & k );
    //==================================================================================================================
    /**
     * Constructor for recreating board from loaded file
     * @param c - color of player
     * @param x - saved X position of figure
     * @param y - saved Y position of figure
     * @param last_x - saved previous X position of figure
     * @param last_y - saved previous Y position of figure
     * @param alive - saved value if figure was alive
     * @param first_move - saved value checking if king performed his first move
     * @param prev_first_move  - previous value checking if king performed his first move ( for undo function )
     */
    King                                                                        (   const COLOR & c,
                                                                                    const unsigned & x,
                                                                                    const unsigned & y,
                                                                                    const unsigned & last_x,
                                                                                    const unsigned & last_y ,
                                                                                    const bool & alive,
                                                                                    const bool & first_move,
                                                                                    const bool & prev_first_move );
    //==================================================================================================================
    /**
    * Set position for figure
    * @param x - new X position
    * @param y - new Y position
    * @return true if position was valid and set, false if not
    */
    bool setPosition                                                            (   const unsigned & x,
                                                                                    const unsigned & y ) override;
    //==================================================================================================================
    /**
    * Check if figure threaten certain position
    * @param x - X position to threaten
    * @param y - Y position to threaten
    * @return - true if threaten, false if not
    */
    bool threatenPosition                                                       (   unsigned x,
                                                                                    unsigned y ) override;
    //==================================================================================================================
    /**
     * Checks if king performed his first move
     * @return value of first_move
     */
    bool wasMoved                                                               () const;
    //==================================================================================================================
    /**
     * Generate all possible moves ( even not valid on current chessboard )
     * @return map of possible moves
     */
    std::set<std::pair<std::string, unsigned >>    calculateAllPossibleMoves    () override;
    //==================================================================================================================
    /**
     * Undo last move and return to state before
     */
    void undoMove                                                               () override;
    //==================================================================================================================
    /**
     * Return string with attributes of figure for saving in file
     * @return
     */
    std::string saveFigure                        () override ;
    //==================================================================================================================
    /**
     * Copy function
     * @return copied object
     */
    King* Copy() override;
    //==================================================================================================================

    private:
        bool first_move;
        bool prev_first_move;
};