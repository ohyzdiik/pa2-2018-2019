## Popis

Semestrálna práca bude obsahovať implementované klasické šachy.
Bude obsahovať triedu Figure, ktorú bude dediť každá trieda nejakej konkrétnej figúrky.
V rámci týchto tried sa bude kontrolovať validita ťahu pre figúrku, meno a či je figúrka nažive.
Šachovnica bude implementovaná v triede Game, kde bude pre bieleho aj čierneho hráča vlastná štruktúra.
V rámci triedy Game sa bude riešiť vyhadzovanie figuriek ( celkovo pravidlá šachu ). Ďalšim krokom bude implementácia hry dvoch hráčov alebo proti umelej inteligencií. Posledným bodom bude implementácia grafiky cez ncurses.

# Šachy

Klasická hra Šachy (příp. libovolná varianta - asijské šachy, ...)

Implementujte následující varianty:

* pro 2 hráče na jednom počítači
* pro hru proti počítači

Hra musí splňovat následující funkcionality:

* Dodržování všech pravidel dané varianty (u klasické varianty tedy i rošáda, braní mimochodem, proměna pěšce na dámu).
* Ukládání (resp. načítání) rozehrané hry do (resp. ze) souboru (vytvořte vhodný formát a uživatelské rozhraní)
* Oznamovat konec hry (šach, mat, pat) a její výsledek.
* umělá inteligence (škálovatelná nebo alespoň 3 různé druhy, jeden druh můžou být náhodné tahy, ale nestačí implementovat pouze náhodné tahy)

Kde lze využít polymorfismus? (doporučené)

* Ovládání hráčů: lokální hráč, umělá inteligence (různé druhy), síťový hráč
* Pohyby figurek: král, dáma, věž, kůň,...
* Uživatelské rozhraní: konzolové, ncurses, SDL, OpenGL (různé druhy),...
* Pravidla různých variant: klasické šachy, žravé šachy, asijské šachy
* Jednotlivá pravidla: tahy, rošáda, braní mimochodem, proměna (jejich výběr pak může být konfigurovatelný)

Další informace

* https://cs.wikipedia.org/wiki/Šachy
* https://cs.wikipedia.org/wiki/Šachové_varianty

