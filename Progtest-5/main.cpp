#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;

// your code will be compiled in a separate namespace
namespace MysteriousNamespace {
#endif /* __PROGTEST__ */

//==============================================================================================

struct Mail_Operations {

    string operation;

    string value;

    Mail_Operations()
    {
        operation   = "";
        value       = "";
    }
};

//==============================================================================================


struct compareByLength {

    bool operator()( const CTimeStamp & a, const CTimeStamp & b ) const {

        return ( a.Compare(b) < 0 );
    }

};

class CMailLog
{
  public:

                    CMailLog                                ()
                    {
                        this->log_size = 0;

                    }

    int             ParseLog                                ( istream          & in );

    list<CMail>     ListMail                                ( const CTimeStamp & from,
                                                              const CTimeStamp & to ) const;

    set<string>     ActiveUsers                             ( const CTimeStamp & from,
                                                              const CTimeStamp & to ) const;

  private:

    multimap<CTimeStamp, CMail , compareByLength> mail_log;

    map<string, pair<Mail_Operations,Mail_Operations>> temp;

    multimap<string, string> to_saved;

    multimap<string, CTimeStamp> time_saved;

    int log_size;

    int convertStringMonthToInt( const string & month );


};

    //----------------------------------------------------------------------------------------

    int             CMailLog::convertStringMonthToInt                               (const string &month)
    {

        if( month == "Jan" )
            return  1;

        if( month == "Feb")
            return 2;

        if( month ==  "Mar")
            return 3;

        if( month == "Apr")
            return 4;

        if( month == "May")
            return 5;

        if(month == "Jun")
            return 6;

        if( month ==  "Jul")
            return 7;

        if( month == "Aug")
            return 8;

        if( month ==  "Sep" || month == "Sept" )
            return 9;

        if( month == "Oct")
            return 10;

        if( month == "Nov")
            return 11;

        if( month == "Dec")
            return 12;


        return 0;
    }


    //----------------------------------------------------------------------------------------

    int             CMailLog::ParseLog                                              (istream &in)
    {
        int size = 0;

        do {

            if(in.eof())
                break;

            string month;
            string day;
            string year;
            string hour;
            string minute;
            string second;
            string server;
            string ID;
            string msg;

            getline(in, month, ' ');

            if( month == "")
            {
                break;
            }

            getline(in, day, ' ');
            getline(in, year, ' ');

            getline(in, hour, ':');
            getline(in, minute, ':');
            getline(in, second, ' ');

            getline(in, server, ' ');
            getline(in, ID, ':');

            getline(in, msg, '\n');

            msg.erase(std::remove(msg.begin(), msg.end(), '\n'), msg.end());

            int i_month     = convertStringMonthToInt(month);
            int i_day       = stoi(day);
            int i_year      = stoi(year);

            int i_hour      = stoi(hour);
            int i_minute    = stoi(minute);
            double i_second = stod(second);

            if( msg != " mail undeliverable")
            {
                istringstream tmp;

                string operation;
                string message;

                tmp . clear ();

                tmp . str (msg);

                getline(tmp, operation, '=');
                getline(tmp, message);

                if(month == " ")
                {
                    break;
                }

                map<string, pair<Mail_Operations,Mail_Operations>>::iterator i = temp.find(ID);

                if( i == temp.end() )
                {
                    if( operation != " from")
                    {
                        break;
                    }

                    Mail_Operations from_log;

                    from_log.operation = operation;
                    from_log.value     = message;

                    Mail_Operations sub_log;

                    temp.emplace(make_pair(ID, make_pair(from_log,sub_log)));
                }

                if ( operation == " subject" )
                {
                    Mail_Operations sub_log;

                    sub_log.operation       = operation;
                    sub_log.value           = message;

                    i->second.second         = sub_log;


                }

                if( operation == " to" )
                {
                    CTimeStamp tmp_time(i_year, i_month, i_day, i_hour, i_minute, i_second );

                    CTimeStamp * time_ins;

                    time_ins = &tmp_time;

                    string * message_tmp;

                    message_tmp = &message;

                    auto time = time_saved.emplace(make_pair(ID, *time_ins));

                    auto dest = to_saved.emplace(make_pair(ID, *message_tmp));

                    CMail log_tmp( time->second , i->second.first.value, dest->second , i->second.second.value );

                    CMail * log;

                    log = &log_tmp;

                    mail_log.emplace(make_pair(time->second, *log));

                    size++;
                }

            }


        } while( !in.eof()  );


        return size;
    }

    //----------------------------------------------------------------------------------------

    list<CMail>     CMailLog::ListMail                                              ( const CTimeStamp &from,
                                                                                      const CTimeStamp &to ) const
    {
            list<CMail> output;

            for( const auto & x: this->mail_log )
            {
                if( x.first.Compare(from) >= 0 && x.first.Compare(to) <= 0 )
                {
                    output.push_back(x.second);

                } else if( x.first.Compare(to) > 0 )
                {
                    break;
                }

            }



            return output;
    }

    //----------------------------------------------------------------------------------------

    set<string>     CMailLog::ActiveUsers                                           (const CTimeStamp &from,
                                                                                     const CTimeStamp &to) const
    {
            set<string> output;

            for( const auto & x: this->mail_log )
            {
                if( x.first.Compare(from) >= 0 && x.first.Compare(to) <= 0 )
                {
                    output.emplace(x.second.To());
                    output.emplace(x.second.From());

                } else if( x.first.Compare(to) > 0 )
                {
                    break;
                }

            }

            return output;
    }


    //----------------------------------------------------------------------------------------


//==============================================================================================

#ifndef __PROGTEST__
} // namespace
string             printMail                               ( const list<CMail> & all )
{ 
  ostringstream oss;

  for ( const auto & mail : all )
  {
      oss << mail << endl;

  }

  return oss . str ();
}
string             printUsers                              ( const set<string> & all )
{
  ostringstream oss;
  bool first = true;
  for ( const auto & name : all )
  {
    if ( ! first )
      oss << ", ";
    else 
      first = false;

    oss << name;

  }

  return oss . str ();
}
int                main                                    ( void )
{
   MysteriousNamespace::CMailLog m;

   list<CMail> mailList;

   set<string> users;

   istringstream iss;

   iss . clear ();

   iss . str (
     "Mar 29 2019 12:35:32.233 relay.fit.cvut.cz ADFger72343D: from=user1@fit.cvut.cz\n"
     "Mar 29 2019 12:37:16.234 relay.fit.cvut.cz JlMSRW4232Df: from=person3@fit.cvut.cz\n"
     "Mar 29 2019 12:55:13.023 relay.fit.cvut.cz JlMSRW4232Df: subject=New progtest homework!\n"
     "Mar 29 2019 13:38:45.043 relay.fit.cvut.cz Kbced342sdgA: from=office13@fit.cvut.cz\n"
     "Mar 29 2019 13:36:13.023 relay.fit.cvut.cz JlMSRW4232Df: to=user76@fit.cvut.cz\n"
     "Mar 29 2019 13:55:31.456 relay.fit.cvut.cz KhdfEjkl247D: from=PR-department@fit.cvut.cz\n"
     "Mar 29 2019 14:18:12.654 relay.fit.cvut.cz Kbced342sdgA: to=boss13@fit.cvut.cz\n"
     "Mar 29 2019 14:48:32.563 relay.fit.cvut.cz KhdfEjkl247D: subject=Business partner\n"
     "Mar 29 2019 14:58:32.000 relay.fit.cvut.cz KhdfEjkl247D: to=HR-department@fit.cvut.cz\n"
     "Mar 29 2019 14:25:23.233 relay.fit.cvut.cz ADFger72343D: mail undeliverable\n"
     "Mar 29 2019 15:02:34.231 relay.fit.cvut.cz KhdfEjkl247D: to=CIO@fit.cvut.cz\n"
     "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=CEO@fit.cvut.cz\n"
     "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=dean@fit.cvut.cz\n"
     "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=vice-dean@fit.cvut.cz\n"
     "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=archive@fit.cvut.cz\n"
     );

   assert ( m . ParseLog ( iss ) == 8 );

   mailList = m . ListMail ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),

                             CTimeStamp ( 2019, 3, 29, 23, 59, 59 ) );


  assert ( printMail ( mailList ) ==
    "2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
    "2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
    "2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> CEO@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> dean@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> vice-dean@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> archive@fit.cvut.cz, subject: Business partner\n"
    "2019-03-29 15:02:34.231 PR-department@fit.cvut.cz -> CIO@fit.cvut.cz, subject: Business partner\n"
    );


  mailList = m . ListMail ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 29, 14, 58, 32 ) );

  assert ( printMail ( mailList ) ==
    "2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
    "2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
    "2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n" );


  mailList = m . ListMail ( CTimeStamp ( 2019, 3, 30, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 30, 23, 59, 59 ) );

  assert ( printMail ( mailList ) == "" );

  users = m . ActiveUsers ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 29, 23, 59, 59 ) );

  assert ( printUsers ( users ) == "CEO@fit.cvut.cz, CIO@fit.cvut.cz, HR-department@fit.cvut.cz, PR-department@fit.cvut.cz, archive@fit.cvut.cz, boss13@fit.cvut.cz, dean@fit.cvut.cz, office13@fit.cvut.cz, person3@fit.cvut.cz, user76@fit.cvut.cz, vice-dean@fit.cvut.cz" );
  users = m . ActiveUsers ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                            CTimeStamp ( 2019, 3, 29, 13, 59, 59 ) );
  assert ( printUsers ( users ) == "person3@fit.cvut.cz, user76@fit.cvut.cz" );
  return 0;
}
#endif /* __PROGTEST__ */
