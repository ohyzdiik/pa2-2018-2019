#ifndef __PROGTEST__
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <deque>
#include <map>
#include <set>
#include <list>
#include <algorithm>

#if defined ( __cplusplus ) && __cplusplus > 199711L /* C++ 11 */
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <functional>
#endif /* C++ 11 */

using namespace std;
#endif /* __PROGTEST__ */

struct default_function {
    template <typename _E>
    bool operator() (const _E & ) const { return true; }

};

class NoRouteException 
{ 
};

template <typename _T, typename _E>
class Node
{
    public:
        _T a;
        _T b;
        _E parameter;

        Node() {

        };

        Node(   const _T & u1,
                const _T & u2,
                const _E & h ): a(u1), b(u2), parameter(h) {}
};

template <typename _T, typename _E>
class CRoute
{
  public:

    //------------------------------------------------------------------------------
    CRoute          () = default;
   //------------------------------------------------------------------------------
   CRoute&  Add     ( const _T & u1,
                      const _T & u2,
                      const _E & h )
   {

       if( this->route_map.find(u1) == this->route_map.end() )
       {
           this->route_map.emplace( make_pair( u1 , list <Node<_T,_E>> { Node<_T,_E>(u1,u2,h) }));

       } else{

           this->route_map[u1].push_back(Node<_T,_E>(u1,u2,h));
       }

       if( this->route_map.find(u2) == this->route_map.end() )
       {
           this->route_map.emplace( make_pair( u2 , list <Node<_T,_E>> { Node<_T,_E>(u2,u1,h) }));

       } else{

           this->route_map[u2].push_back(Node<_T,_E>(u2,u1,h));
       }


       return *this;
   }

    //------------------------------------------------------------------------------
    template<typename FUNCTION = default_function>
    list<_T> Find(const _T & u1, const _T & u2, const FUNCTION & f = default_function() ) {

        if( u1 == u2 )
        {
            return list<_T>{u1};
        }

       if( this->route_map.find(u1) == this->route_map.end() )
        {
            throw NoRouteException();
        }

        map <_T, list<_T>> route;

        queue<_T> opened;
        set<_T> closed;

        opened.push(u1);
        closed.emplace(u1);

        route.emplace(make_pair(u1, list<_T>{}));

        while(!opened.empty())
        {
            for( auto i : this->route_map.at(opened.front()) )
            {
                if( closed.find(i.b) == closed.end() && f(i.parameter))
                {
                    closed.emplace(i.b);
                    route.emplace(make_pair(i.b, route[opened.front()]));
                    route[i.b].push_back(opened.front());
                    opened.push(i.b);
                }

                if( i.b == u2 )
                {
                    route[i.b].push_back(i.b);

                    return route[i.b];
                }
            }

            opened.pop();
        }

        throw NoRouteException();
    }

    //------------------------------------------------------------------------------

  private:

    map<_T, list<Node<_T,_E>> > route_map;
};

#ifndef __PROGTEST__
//=================================================================================================
class CTrain
{
  public:
                             CTrain                        ( const string    & company, 
                                                             int               speed )
                             : m_Company ( company ), 
                               m_Speed ( speed ) 
    { 
    }
    //---------------------------------------------------------------------------------------------
    string                   m_Company;
    int                      m_Speed; 
};
//=================================================================================================
class TrainFilterCompany
{
  public:
                             TrainFilterCompany            ( const set<string> & companies ) 
                             : m_Companies ( companies ) 
    { 
    }
    //---------------------------------------------------------------------------------------------
    bool                     operator ()                   ( const CTrain & train ) const
    { 
      return m_Companies . find ( train . m_Company ) != m_Companies . end (); 
    }
    //---------------------------------------------------------------------------------------------
  private:  
    set <string>             m_Companies;    
};
//=================================================================================================
class TrainFilterSpeed
{
  public:
                             TrainFilterSpeed              ( int               min, 
                                                             int               max )
                             : m_Min ( min ), 
                               m_Max ( max ) 
    { 
    }
    //---------------------------------------------------------------------------------------------
    bool                     operator ()                   ( const CTrain    & train ) const
    { 
      return train . m_Speed >= m_Min && train . m_Speed <= m_Max; 
    }
    //---------------------------------------------------------------------------------------------
  private:  
    int                      m_Min;
    int                      m_Max; 
};
//=================================================================================================
bool               NurSchnellzug                           ( const CTrain    & zug )
{
  return ( zug . m_Company == "OBB" || zug . m_Company == "DB" ) && zug . m_Speed > 100;
}
//=================================================================================================
static string      toText                                  ( const list<string> & l )
{
  ostringstream oss;
  
  auto it = l . cbegin();
  oss << *it;
  for ( ++it; it != l . cend (); ++it )
    oss << " > " << *it;
  return oss . str ();
}
//=================================================================================================
int main ( void )
{
  CRoute<string,CTrain> lines;
  
      lines . Add ( "Berlin", "Prague", CTrain ( "DB", 120 ) )
            . Add ( "Berlin", "Prague", CTrain ( "CD",  80 ) )
            . Add ( "Berlin", "Dresden", CTrain ( "DB", 160 ) )
            . Add ( "Dresden", "Munchen", CTrain ( "DB", 160 ) )
            . Add ( "Munchen", "Prague", CTrain ( "CD",  90 ) )
            . Add ( "Munchen", "Linz", CTrain ( "DB", 200 ) )
            . Add ( "Munchen", "Linz", CTrain ( "OBB", 90 ) )
            . Add ( "Linz", "Prague", CTrain ( "CD", 50 ) )
            . Add ( "Prague", "Wien", CTrain ( "CD", 100 ) )
            . Add ( "Linz", "Wien", CTrain ( "OBB", 160 ) )
            . Add ( "Paris", "Marseille", CTrain ( "SNCF", 300 ))
            . Add ( "Paris", "Dresden",  CTrain ( "SNCF", 250 ) );
  list<string> r1 = lines . Find ( "Berlin", "Linz" );
  assert ( toText ( r1 ) == "Berlin > Prague > Linz" );

  list<string> r2 = lines . Find ( "Linz", "Berlin" );
  assert ( toText ( r2 ) == "Linz > Prague > Berlin" );

  list<string> r3 = lines . Find ( "Wien", "Berlin" );
  assert ( toText ( r3 ) == "Wien > Prague > Berlin" );

  list<string> r4 = lines . Find ( "Wien", "Berlin", NurSchnellzug );
  assert ( toText ( r4 ) == "Wien > Linz > Munchen > Dresden > Berlin" );

  list<string> r5 = lines . Find ( "Wien", "Munchen", TrainFilterCompany ( set<string> { "CD", "DB" } ) );
  assert ( toText ( r5 ) == "Wien > Prague > Munchen" );

  list<string> r6 = lines . Find ( "Wien", "Munchen", TrainFilterSpeed ( 120, 200 ) );
  assert ( toText ( r6 ) == "Wien > Linz > Munchen" );

  list<string> r7 = lines . Find ( "Wien", "Munchen", [] ( const CTrain & x ) { return x . m_Company == "CD"; } );
  assert ( toText ( r7 ) == "Wien > Prague > Munchen" );

  list<string> r8 = lines . Find ( "Munchen", "Munchen" );
  assert ( toText ( r8 ) == "Munchen" );

  list<string> r9 = lines . Find ( "Marseille", "Prague" );
  assert ( toText ( r9 ) == "Marseille > Paris > Dresden > Berlin > Prague"
           || toText ( r9 ) == "Marseille > Paris > Dresden > Munchen > Prague" );

  try
  {
    list<string> r10 = lines . Find ( "Marseille", "Prague", NurSchnellzug );
    assert ( "Marseille > Prague connection does not exist!!" == NULL );
  }
  catch ( const NoRouteException & e )
  {
  }

  list<string> r11 = lines . Find ( "Salzburg", "Salzburg" );
  assert ( toText ( r11 ) == "Salzburg" );

  list<string> r12 = lines . Find ( "Salzburg", "Salzburg", [] ( const CTrain & x ) { return x . m_Company == "SNCF"; }  );
  assert ( toText ( r12 ) == "Salzburg" );

  try
  {
    list<string> r13 = lines . Find ( "London", "Oxford" );
    assert ( "London > Oxford connection does not exist!!" == NULL );
  }
  catch ( const NoRouteException & e )
  {
  }
  return 0;
}
#endif  /* __PROGTEST__ */
