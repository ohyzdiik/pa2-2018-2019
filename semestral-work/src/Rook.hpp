#pragma once

#include "Figure.hpp"

class Rook : public Figure
{
public:

    //==================================================================================================================
    /**
    * Constructor for initialization of chessboard
    * @param x - starting X position
    * @param y - starting Y position
    * @param color - color of player
    */
    Rook                                                                        (   const unsigned & x,
                                                                                    const unsigned & y,
                                                                                    const COLOR & color );
    //==================================================================================================================
    /**
     * Empty constructor
     */
    Rook                                                                        ();
    //==================================================================================================================
    /**
      * Constructor for recreating board from loaded file
     * @param c - color of player
     * @param x - saved X position of figure
     * @param y - saved Y position of figure
     * @param last_x - saved previous X position of figure
     * @param last_y - saved previous Y position of figure
     * @param alive - saved value if figure was alive
     * @param first_move - saved value if rook performed first move
     * @param last_first_move - previous value if rook performed first move
     */
    Rook                                                                        (   const COLOR & c,
                                                                                    const unsigned & x,
                                                                                    const unsigned & y,
                                                                                    const unsigned & last_x,
                                                                                    const unsigned & last_y ,
                                                                                    const bool & alive,
                                                                                    const bool & first_move,
                                                                                    const bool & last_first_move );
    //==================================================================================================================
    /**
    * Copy constructor
    * @param r - object from copy
    */
    Rook                                                                        ( const Rook & r );
    //==================================================================================================================
    /**
    * Overloaded operator =
    * @param r - object to copy from
    * @return - copied object
    */
    Rook & operator=                                                            ( const Rook & r );
    //==================================================================================================================
    Rook* Copy() override;
    //==================================================================================================================
    bool setPosition                                                            (   const unsigned & x,
                                                                                    const unsigned & y ) override;
    //==================================================================================================================
    bool threatenPosition                                                       (   unsigned x,
                                                                                    unsigned y) override;
    //==================================================================================================================
    /**
     * Check if rook performed first move
     * @return - value if first_move
     */
    bool wasMoved                                                               () const;
    //==================================================================================================================
    std::set< std::pair<std::string, unsigned > >    calculateAllPossibleMoves  () override;
    //==================================================================================================================
    void undoMove                                                               () override ;
    //==================================================================================================================
    std::string saveFigure                                                      () override ;
    //==================================================================================================================
    private:
        bool first_move;
        bool last_first_move;
    //==================================================================================================================
};