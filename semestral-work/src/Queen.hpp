#pragma once

#include "Figure.hpp"

class Queen : public Figure
{

public:
    //==================================================================================================================
    /**
    * Constructor for initialization of chessboard
    * @param x - starting X position
    * @param y - starting Y position
    * @param color - color of player
    */
    Queen                                                                       (   const unsigned & x,
                                                                                    const unsigned & y,
                                                                                    const COLOR & color);
    //==================================================================================================================
    /**
      * Constructor for recreating board from loaded file
      * @param c - color of player
      * @param x - saved X position of figure
      * @param y - saved Y position of figure
      * @param last_x - saved previous X position of figure
      * @param last_y - saved previous Y position of figure
      * @param alive - saved value if figure was alive
      */
    Queen                                                                        (  const COLOR & c,
                                                                                    const unsigned & x,
                                                                                    const unsigned & y,
                                                                                    const unsigned & last_x,
                                                                                    const unsigned & last_y ,
                                                                                    const bool & alive );
    //==================================================================================================================
    /**
    * Copy constructor
    * @param q - object from copy
    */
    Queen                                                                       ( const Queen & q );
    //==================================================================================================================
    /**
     * Empty constructor
     */
    Queen                                                                       () ;
    //==================================================================================================================
    /**
    * Overloaded operator =
    * @param q - object to copy from
    * @return - copied object
    */
    Queen& operator=                                                            ( const Queen & q );
    //==================================================================================================================
    bool setPosition                                                            (   const unsigned & x,
                                                                                    const unsigned & y ) override;
    //==================================================================================================================
    bool threatenPosition                                                       (   unsigned x,
                                                                                    unsigned y) override;
    //==================================================================================================================
    std::set<std::pair<std::string, unsigned >>    calculateAllPossibleMoves    () override;
    //==================================================================================================================
    Queen* Copy() override;
    //==================================================================================================================
};