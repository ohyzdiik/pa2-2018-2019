#pragma once

#include "Figure.hpp"

class Knight : public Figure
{
public:

    //==================================================================================================================
    /**
    * Constructor for initialization of chessboard
    * @param x - starting X position
    * @param y - starting Y position
    * @param color - color of player
    */
    Knight                                                                      (   const unsigned & x,
                                                                                    const unsigned & y,
                                                                                    const COLOR & color );
    //==================================================================================================================
    /**
     * Empty constructor
     */
    Knight                                                                      ();
    //==================================================================================================================
    /**
     * Constructor for recreating board from loaded file
     * @param c - color of player
     * @param x - saved X position of figure
     * @param y - saved Y position of figure
     * @param last_x - saved previous X position of figure
     * @param last_y - saved previous Y position of figure
     * @param alive - saved value if figure was alive
     */
    Knight                                                                        (     const COLOR & c,
                                                                                        const unsigned & x,
                                                                                        const unsigned & y,
                                                                                        const unsigned & last_x,
                                                                                        const unsigned & last_y ,
                                                                                        const bool & alive);
    //==================================================================================================================
    /**
    * Copy constructor
    * @param k - object from copy
    */
    Knight                                                                      ( const Knight & k );
    //==================================================================================================================
    /**
    * Overloaded operator =
    * @param k - object to copy from
    * @return - copied object
    */
    Knight & operator=                                                          ( const Knight & k );
    //==================================================================================================================
    /**
     * Set position for figure
     * @param x - new X position
     * @param y - new Y position
     * @return true if position was valid and set, false if not
     */
    bool setPosition                                                            (   const unsigned & x,
                                                                                    const unsigned & y ) override;
    //==================================================================================================================
    /**
     * Check if figure threaten certain position
     * @param x - X position to threaten
     * @param y - Y position to threaten
     * @return - true if threaten, false if not
     */
    bool threatenPosition                                                       (   unsigned x,
                                                                                    unsigned y ) override;
    //==================================================================================================================
    /**
    * Generate all possible moves ( even not valid on current chessboard )
    * @return map of possible moves
    */
    std::set<std::pair<std::string, unsigned >>    calculateAllPossibleMoves   () override;
    //==================================================================================================================
    /**
   * Generate all possible moves ( even not valid on current chessboard )
   * @return map of possible moves
   */
    Knight* Copy() override;
    //==================================================================================================================

};