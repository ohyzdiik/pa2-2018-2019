#include "Multi.hpp"

//======================================================================================================================
Multi::Multi() {

    this->startGame();
}
//======================================================================================================================
void Multi::Play() {

    std::string command;
    COLOR color = WHITE;

    while( true )
    {
        std::cout << "---------------------------------------------" << std::endl;
        this->Print();
        std::cout << "---------------------------------------------" << std::endl;

        if ( color == WHITE )
            std::cout << "White goes: " << std::endl;
        else
            std::cout << "Black goes: " << std::endl;

        std::cout << "Save - [ SAVE GAME ] | Move - [ MAKE MOVE ] | Exit - [EXIT GAME]" << std::endl;

        std::cin >> command;

        int command_out = this->parseCommand(command, color);

        while( command_out == -1 || command_out == 2 )
        {
            if( command_out == -1 )
                std::cout << "Invalid command" << std::endl;

            std::cout << "Save - [ SAVE GAME ] | Move - [ MAKE MOVE ] | Exit - [EXIT GAME]" << std::endl;

            std::cin.clear();
            std::cin.ignore(1024, '\n');

            std::cin >> command;

            command_out = this->parseCommand(command, color);
        }

        std::cout << "COMMAND OUTPUT " << command_out << std::endl;
        if ( command_out == 0 )
        {
            break;
        }

        if ( color == WHITE )
            color = BLACK;
        else
            color = WHITE;
    }
}
//======================================================================================================================
