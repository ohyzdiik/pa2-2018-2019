#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;
#endif /* __PROGTEST__ */

//======================================================================================================================
class CComponent {

    public:

        CComponent                          () = default;

        CComponent                          ( const CComponent & c );

        virtual ~CComponent                 () = default;

        virtual CComponent *    Copy        () const;

        virtual  string         toString    ( string prefix );

    protected:

        string name;

};

//----------------------------------------------------------------------------------------------------------------------
/**
 * Default constructor
 * @param c
 */
CComponent::CComponent(const CComponent &c) {
    this->name = c.name;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Get copy of class instance
 * @return new CComponent
 */
CComponent* CComponent::Copy() const {

    return new CComponent(*this);

}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Return string of component
 * @param prefix
 * @return string out
 */
string CComponent::toString(string prefix) {

    string out = prefix + this->name;

    return out;
}
//======================================================================================================================
class CComputer{

    public:

        CComputer                               ();

        CComputer                               ( const string name );

        ~CComputer                              ();

        CComputer                               ( const CComputer & c );

        CComputer&      operator=               ( const CComputer & c );

        CComputer&      AddAddress              ( const string & addr );

        CComputer&      AddComponent            ( const CComponent  & component );

        string GetName                          () const;

        string toString                         ( const string & prefix ) const;

        friend ostream & operator<<  (ostream &oss, const CComputer &c) {

            oss << c.toString("");

            return oss;
        }

    private:

        vector<CComponent*> pc_components;
        string name;
        vector<string> address;
};
//----------------------------------------------------------------------------------------------------------------------
/**
 * Empty constructor
 */
CComputer::CComputer() {
    this->name = "";
    this->pc_components.clear();
    this->address.clear();
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Default constructor
 * @param name
 */
CComputer::CComputer(const string name) {
    this->name  = name;
    this->pc_components.clear();
    this->address.clear();
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Destructor
 */
CComputer::~CComputer() {

    this->name = "";

    this->address.clear();
    this->address.shrink_to_fit();

    for( auto i : this->pc_components )
    {
        delete i;
    }

    this->pc_components.clear();
    this->pc_components.shrink_to_fit();
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Copy constructor
 * @param c
 */
CComputer::CComputer(const CComputer &c) {

    this->name = c.name;

    this->address.clear();

    unsigned  addr_size = c.address.size();

    for( unsigned i = 0; i < addr_size; i++  )
    {
        this->address.push_back(c.address[i]);
    }

    this->pc_components.clear();

    unsigned component_size  = c.pc_components.size();

    for( unsigned i = 0; i < component_size; i++ )
    {
        this->pc_components.push_back(c.pc_components[i]->Copy());
    }
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Overloaded operator =
 * @param c
 */
CComputer& CComputer::operator=(const CComputer &c) {

    this->name = c.name;

    this->address.clear();

    unsigned  addr_size = c.address.size();

    for( unsigned i = 0; i < addr_size; i++  )
    {
        this->address.push_back(c.address[i]);
    }

    this->pc_components.clear();

    unsigned component_size = c.pc_components.size();

    for( unsigned i = 0; i < component_size; i++ )
    {
        this->pc_components.push_back(c.pc_components[i]->Copy());
    }

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Add address to computer list
 * @param addr
 * @return
 */
CComputer& CComputer::AddAddress(const string &addr) {

    this->address.push_back(addr);

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Add component to pc component's list
 * @param component
 * @return current class
 */
CComputer& CComputer::AddComponent(const CComponent &component) {

    this->pc_components.push_back(component.Copy());

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Getter of computer name
 * @return computer name
 */
string CComputer::GetName() const {
    return this->name;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Get computer info in string by certain prefix
 * @param prefix
 * @return host string
 */
string CComputer::toString(const string &prefix) const {

    string host = prefix + "Host: " + this->name + "\n";

    string host_prefix;

    string component_prefix;

    if( prefix == "" )
    {
        host_prefix = "+-";

        component_prefix="";

    } else if( prefix == "+-" )
    {
        host_prefix = "| +-";
        component_prefix = "| ";

    } else {

        host_prefix = "  +-";
        component_prefix = "  ";
    }


    for(string addr : this->address )
    {
        host+=host_prefix + addr + "\n";
    }

    unsigned components_size = this->pc_components.size();

    for( unsigned i = 0; i < components_size; i++ )
    {
        if( i == (components_size - 1) )
        {
            host+=  (*this->pc_components[i]).toString(component_prefix + "\\-") ;

        } else {

            host+= (*this->pc_components[i]).toString(component_prefix + "+-");

        }
    }

    return host;
}
//======================================================================================================================
class CNetwork{

    public:

        CNetwork                                ();

        CNetwork                                ( const string & name );

        ~CNetwork                               ();

        CNetwork                                ( const CNetwork & c );

        CNetwork &      operator=               ( const CNetwork & c );

        CNetwork &      AddComputer             ( const CComputer & c );

        CComputer *     FindComputer            ( const string & name );

        friend ostream & operator<<   ( ostream &oss, const CNetwork &c ) {

        oss << "Network: "  << c.name << "\n";

        unsigned  network_size = c.network_components.size();

        for( unsigned i = 0; i < network_size; i++  )
        {
            if( i == ( network_size - 1) )
            {
                oss << c.network_components[i].toString("\\-");

            } else {

                oss << c.network_components[i].toString("+-");
            }
        }

        return oss;
    }

    private:

        vector<CComputer> network_components;

        string name;

};
//----------------------------------------------------------------------------------------------------------------------
/**
 * Empty constructor
 */
CNetwork::CNetwork() {
    this->name = "";
    this->network_components.clear();
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Default constructor
 * @param name
 */
CNetwork::CNetwork(const string &name) {
    this->name = name;
    this->network_components.clear();
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Destructor
 */
CNetwork::~CNetwork() {

    this->network_components.clear();
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Copy constructor
 * @param c
 */
CNetwork::CNetwork(const CNetwork &c) {

    this->name = c.name;

    this->network_components.clear();

    unsigned network_size = c.network_components.size();

    for( unsigned i = 0; i < network_size; i++ )
    {
        this->network_components.push_back(c.network_components[i]);
    }
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Overloaded operator =
 * @param c
 * @return
 */
CNetwork& CNetwork::operator=(const CNetwork &c) {

    this->name = c.name;

    this->network_components.clear();

    unsigned network_size = c.network_components.size();

    for( unsigned i = 0; i < network_size; i++ )
    {
        this->network_components.push_back(c.network_components[i]);
    }

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Add computer to network
 * @param c
 */
CNetwork& CNetwork::AddComputer(const CComputer &c) {

    CComputer tmp(c);

    this->network_components.push_back(tmp);

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Find computer by name
 * @param name
 * @return instance of ccomputer if found, if not nullptr
 */
CComputer* CNetwork::FindComputer(const string &name) {

    unsigned n_size = this->network_components.size();

    for( unsigned i = 0; i < n_size; i++ )
    {
        if( this->network_components[i].GetName() == name )
        {
            return &(this->network_components[i]);
        }
    }

    return nullptr;
}
//======================================================================================================================
class CCPU: public CComponent {

    public:

        CCPU                            ();

        CCPU                            ( const unsigned & freq ,
                                          const unsigned & core_count );

        CCPU                            ( const CCPU & c );

        ~CCPU                           () = default;

        CCPU &  operator=               ( const CCPU & c );

        CCPU * Copy                     () const override;

        string toString                 (  string prefix ) override;

    private:

        unsigned freq;

        unsigned core_count;

};
//----------------------------------------------------------------------------------------------------------------------
/**
 * Empty constructor
 */
CCPU::CCPU() {

    this->name = "CPU";
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Default constructor
 * @param freq
 * @param core_count
 */
CCPU::CCPU(const unsigned &freq, const unsigned &core_count) {

    this->name          = "CPU";
    this->freq          = freq;
    this->core_count    = core_count;

}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Copy constructor
 * @param c
 */
CCPU::CCPU(const CCPU &c) {

    this->core_count = c.core_count;

    this->freq      = c.freq;

    this->name      = c.name;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Overloaded operator =
 * @param c
 */
CCPU& CCPU::operator=(const CCPU &c) {

    this->core_count = c.core_count;

    this->freq      = c.freq;

    this->name      = c.name;

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Return copy of cpu with all properties
 * @return new CDisk
 */
CCPU* CCPU::Copy() const {
    return new CCPU(*this);
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * Return string based on prefix for pc component cpu
 * @param prefix
 * @return string out
 */
string CCPU::toString(string prefix) {

    string out = prefix + this->name + ", " + to_string(this->freq) + " cores @ " + to_string(this->core_count)+ "MHz\n";

    return out;
}
//======================================================================================================================
class CMemory : public CComponent {

    public:
            CMemory                         () = default;

            CMemory                         ( const unsigned & mem_size );

            CMemory                         ( const CMemory & c );

            ~CMemory                        () = default;

            CMemory&  operator=             ( const CMemory & c);

            CMemory * Copy                  () const override;

            string    toString              (  string prefix ) override;

private:
    unsigned mem_size;
};
//----------------------------------------------------------------------------------------------------------------------
/**
 * Default constructor
 * @param mem_size
 */
CMemory::CMemory(const unsigned &mem_size) {
    this->name = "Memory";
    this->mem_size = mem_size;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Copy constructor
 * @param c
 */
CMemory::CMemory(const CMemory &c) {

    this->name = c.name;
    this->mem_size = c.mem_size;

}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Overloaded operator=
 * @param c
 */
CMemory& CMemory::operator=(const CMemory &c) {

    this->name = c.name;
    this->mem_size = c.mem_size;

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Return copy of memory with all properties
 * @return new CMemory
 */
CMemory* CMemory::Copy() const {
    return new CMemory(*this);
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Return string based on prefix for pc component memory
 * @param prefix
 * @return string out
 */
string CMemory::toString(string prefix) {

    string out = prefix + this->name + ", " + to_string(this->mem_size) + " MiB\n";

    return out;
}
//======================================================================================================================
class InvalidDiskType
{

};
//----------------------------------------------------------------------------------------------------------------------
/**
 * Structure for saving information about partition for CDisk
 */
struct Partition
{
    int size;
    string name;
};
//----------------------------------------------------------------------------------------------------------------------
class CDisk : public CComponent {

    public:

    static string MAGNETIC;
    static string SSD;

    CDisk                               ();

    CDisk                               ( const string & type,
                                          const unsigned & size );

    CDisk                               ( const CDisk & c );

    ~CDisk                              ();

    CDisk&  operator=                   ( CDisk & c);

    CDisk & AddPartition                ( const int & size,
                                         const string & name );

    CDisk * Copy                        () const override;

    string  toString                    (  string prefix ) override;

    private:

        unsigned size;
        vector<Partition> disk;
};
//----------------------------------------------------------------------------------------------------------------------
/**
 * Empty constructor
 */
CDisk::CDisk() {
    this->disk.clear();
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Default constructor, if type is not valid, throw InvalidDiskType exception
 * @param type
 * @param size
 */
CDisk::CDisk(const string &type, const unsigned &size) {

    if( type != "SSD" && type != "HDD" )
    {
        throw InvalidDiskType();
    }

    this->name = type;
    this->size = size;
    this->disk.clear();
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Copy constructor
 * @param c
 */
CDisk::CDisk(const CDisk &c) {

    this->name = c.name;
    this->size = c.size;

    this->disk.clear();

    unsigned disk_size = c.disk.size();

    for( unsigned i = 0; i < disk_size; i++ ) {

        this->disk.push_back(c.disk[i]);
    }
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Destructor
 */
CDisk::~CDisk() {

    this->disk.clear();
    this->disk.shrink_to_fit();
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * Overloaded operator=
 * @param c
 */
CDisk& CDisk::operator=(CDisk &c) {

    this->name = c.name;
    this->size = c.size;

    this->disk.clear();
    unsigned disk_size = c.disk.size();

    for( unsigned i = 0; i < disk_size; i++ ) {

        this->disk.push_back(c.disk[i]);
    }

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Addsp partition to disk
 * @param size
 * @param name
 * @return *this for next calling of functions
 */
CDisk& CDisk::AddPartition(const int &size, const string &name) {

    Partition p;
    p.size = size;
    p.name = name;

    this->disk.push_back(p);

    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
/**
 * Return copy of disk with all properties
 * @return new CDisk
 */
CDisk* CDisk::Copy() const {

    return new CDisk(*this);
}

//----------------------------------------------------------------------------------------------------------------------
/**
 * Return string based on prefix for pc component disk
 * @param prefix
 * @return string out
 */
string CDisk::toString(string prefix) {

    string string_disk = prefix + this->name + ", " + to_string(this->size) + " GiB\n";

    string disk_prefix;

    if ( prefix == "| +-" )
    {
        disk_prefix = "| | ";

    } else if ( prefix == "| \\-")
    {
        disk_prefix = "|   ";

    } else if( prefix == "  \\-" )
    {
        disk_prefix = "    ";

    } else if( prefix == "  +-" )
    {
        disk_prefix = "  | ";

    } else if( prefix == "+-" )
    {
        disk_prefix = "| ";
    } else
    {
        disk_prefix = "  ";
    }

    unsigned disk_size = this->disk.size();

    for( unsigned i = 0; i < disk_size; i++ )
    {
        if( i == ( disk_size - 1) )
        {
            string_disk+=disk_prefix+"\\-["+to_string(i)+"]: "+ to_string(this->disk[i].size) + " GiB, " + this->disk[i].name + "\n";

        } else {

            string_disk+=disk_prefix+"+-["+to_string(i)+"]: "+ to_string(this->disk[i].size) + " GiB, " + this->disk[i].name + "\n";
        }
    }


    return string_disk;

}
//======================================================================================================================
string CDisk::SSD       = "SSD";
string CDisk::MAGNETIC  = "HDD";
//======================================================================================================================

#ifndef __PROGTEST__
template<typename _T>
string toString ( const _T & x )
{
  ostringstream oss;
  oss << x;
  return oss . str ();
}

int main ( void )
{
//  CNetwork n ( "FIT network" );
//  n . AddComputer (
//        CComputer ( "progtest.fit.cvut.cz" ) .
//          AddAddress ( "147.32.232.142" ) .
//          AddComponent ( CCPU ( 8, 2400 ) ) .
//          AddComponent ( CCPU ( 8, 1200 ) ) .
//          AddComponent ( CDisk ( CDisk::MAGNETIC, 1500 ) .
//            AddPartition ( 50, "/" ) .
//            AddPartition ( 5, "/boot" ).
//            AddPartition ( 1000, "/var" ) ) .
//          AddComponent ( CDisk ( CDisk::SSD, 60 ) .
//            AddPartition ( 60, "/data" )  ) .
//          AddComponent ( CMemory ( 2000 ) ).
//          AddComponent ( CMemory ( 2000 ) ) ) .
//      AddComputer (
//        CComputer ( "edux.fit.cvut.cz" ) .
//          AddAddress ( "147.32.232.158" ) .
//          AddComponent ( CCPU ( 4, 1600 ) ) .
//          AddComponent ( CMemory ( 4000 ) ).
//          AddComponent ( CDisk ( CDisk::MAGNETIC, 2000 ) .
//            AddPartition ( 100, "/" )   .
//            AddPartition ( 1900, "/data" ) ) ) .
//      AddComputer (
//        CComputer ( "imap.fit.cvut.cz" ) .
//          AddAddress ( "147.32.232.238" ) .
//          AddComponent ( CCPU ( 4, 2500 ) ) .
//          AddAddress ( "2001:718:2:2901::238" ) .
//          AddComponent ( CMemory ( 8000 ) ) );
//
//  cout << toString ( n );
//
//  CNetwork x = n;
//
//  cout << "-------------------------" << endl;
//
//  cout << toString ( x );
//
//  auto c = x . FindComputer ( "imap.fit.cvut.cz" );
//
//  cout << "-------------------------" << endl;
//
//  cout << toString ( *c );
//
//    c -> AddComponent ( CDisk ( CDisk::MAGNETIC, 1000 ) .
//            AddPartition ( 100, "system" ) .
//            AddPartition ( 200, "WWW" ) .
//            AddPartition ( 700, "mail" ) );
//
//    cout << "-------------------------" << endl;
//
//    cout << toString ( x );
//
//    cout << "-------------------------" << endl;
//
//    cout << toString ( n );
//
//    auto d = *c;
//
//    c -> AddComponent ( CDisk ( CDisk::SSD, 3241351 ) .
//            AddPartition ( 5415, "tvoja" ) .
//            AddPartition ( 5415, "mamka" ) );
//
//    cout << "-------------------------" << endl;
//
//    cout << toString ( d );
//
//    cout << "-------------------------" << endl;
//
//    cout << toString ( *c );

  /*assert ( toString ( n ) ==
    "Network: FIT network\n"
    "+-Host: progtest.fit.cvut.cz\n"
    "| +-147.32.232.142\n"
    "| +-CPU, 8 cores @ 2400MHz\n"
    "| +-CPU, 8 cores @ 1200MHz\n"
    "| +-HDD, 1500 GiB\n"
    "| | +-[0]: 50 GiB, /\n"
    "| | +-[1]: 5 GiB, /boot\n"
    "| | \\-[2]: 1000 GiB, /var\n"
    "| +-SSD, 60 GiB\n"
    "| | \\-[0]: 60 GiB, /data\n"
    "| +-Memory, 2000 MiB\n"
    "| \\-Memory, 2000 MiB\n"
    "+-Host: edux.fit.cvut.cz\n"
    "| +-147.32.232.158\n"
    "| +-CPU, 4 cores @ 1600MHz\n"
    "| +-Memory, 4000 MiB\n"
    "| \\-HDD, 2000 GiB\n"
    "|   +-[0]: 100 GiB, /\n"
    "|   \\-[1]: 1900 GiB, /data\n"
    "\\-Host: imap.fit.cvut.cz\n"
    "  +-147.32.232.238\n"
    "  +-2001:718:2:2901::238\n"
    "  +-CPU, 4 cores @ 2500MHz\n"
    "  \\-Memory, 8000 MiB\n" );
  CNetwork x = n;
  auto c = x . FindComputer ( "imap.fit.cvut.cz" );
  assert ( toString ( *c ) ==
    "Host: imap.fit.cvut.cz\n"
    "+-147.32.232.238\n"
    "+-2001:718:2:2901::238\n"
    "+-CPU, 4 cores @ 2500MHz\n"
    "\\-Memory, 8000 MiB\n" );
  c -> AddComponent ( CDisk ( CDisk::MAGNETIC, 1000 ) .
         AddPartition ( 100, "system" ) .
         AddPartition ( 200, "WWW" ) .
         AddPartition ( 700, "mail" ) );
  assert ( toString ( x ) ==
    "Network: FIT network\n"
    "+-Host: progtest.fit.cvut.cz\n"
    "| +-147.32.232.142\n"
    "| +-CPU, 8 cores @ 2400MHz\n"
    "| +-CPU, 8 cores @ 1200MHz\n"
    "| +-HDD, 1500 GiB\n"
    "| | +-[0]: 50 GiB, /\n"
    "| | +-[1]: 5 GiB, /boot\n"
    "| | \\-[2]: 1000 GiB, /var\n"
    "| +-SSD, 60 GiB\n"
    "| | \\-[0]: 60 GiB, /data\n"
    "| +-Memory, 2000 MiB\n"
    "| \\-Memory, 2000 MiB\n"
    "+-Host: edux.fit.cvut.cz\n"
    "| +-147.32.232.158\n"
    "| +-CPU, 4 cores @ 1600MHz\n"
    "| +-Memory, 4000 MiB\n"
    "| \\-HDD, 2000 GiB\n"
    "|   +-[0]: 100 GiB, /\n"
    "|   \\-[1]: 1900 GiB, /data\n"
    "\\-Host: imap.fit.cvut.cz\n"
    "  +-147.32.232.238\n"
    "  +-2001:718:2:2901::238\n"
    "  +-CPU, 4 cores @ 2500MHz\n"
    "  +-Memory, 8000 MiB\n"
    "  \\-HDD, 1000 GiB\n"
    "    +-[0]: 100 GiB, system\n"
    "    +-[1]: 200 GiB, WWW\n"
    "    \\-[2]: 700 GiB, mail\n" );
  assert ( toString ( n ) ==
    "Network: FIT network\n"
    "+-Host: progtest.fit.cvut.cz\n"
    "| +-147.32.232.142\n"
    "| +-CPU, 8 cores @ 2400MHz\n"
    "| +-CPU, 8 cores @ 1200MHz\n"
    "| +-HDD, 1500 GiB\n"
    "| | +-[0]: 50 GiB, /\n"
    "| | +-[1]: 5 GiB, /boot\n"
    "| | \\-[2]: 1000 GiB, /var\n"
    "| +-SSD, 60 GiB\n"
    "| | \\-[0]: 60 GiB, /data\n"
    "| +-Memory, 2000 MiB\n"
    "| \\-Memory, 2000 MiB\n"
    "+-Host: edux.fit.cvut.cz\n"
    "| +-147.32.232.158\n"
    "| +-CPU, 4 cores @ 1600MHz\n"
    "| +-Memory, 4000 MiB\n"
    "| \\-HDD, 2000 GiB\n"
    "|   +-[0]: 100 GiB, /\n"
    "|   \\-[1]: 1900 GiB, /data\n"
    "\\-Host: imap.fit.cvut.cz\n"
    "  +-147.32.232.238\n"
    "  +-2001:718:2:2901::238\n"
    "  +-CPU, 4 cores @ 2500MHz\n"
    "  \\-Memory, 8000 MiB\n" );*/

    CNetwork n ( "FIT network" );
    n . AddComputer (
            CComputer ( "progtest.fit.cvut.cz" ) .
                    AddAddress ( "147.32.232.142" ) .
                    AddComponent ( CCPU ( 8, 2400 ) ) .
                    AddComponent ( CCPU ( 8, 1200 ) ) .
                    AddComponent ( CDisk ( CDisk::MAGNETIC, 1500 ) .
                    AddPartition ( 50, "/" ) .
                    AddPartition ( 5, "/boot" ).
                    AddPartition ( 1000, "/var" ) ) .
                    AddComponent ( CDisk ( CDisk::SSD, 60 ) .
                    AddPartition ( 60, "/data" )  ) .
                    AddComponent ( CMemory ( 2000 ) ).
                    AddComponent ( CMemory ( 2000 ) ) ) .
            AddComputer (
            CComputer ( "edux.fit.cvut.cz" ) .
                    AddAddress ( "147.32.232.158" ) .
                    AddComponent ( CCPU ( 4, 1600 ) ) .
                    AddComponent ( CMemory ( 4000 ) ).
                    AddComponent ( CDisk ( CDisk::MAGNETIC, 2000 ) .
                    AddPartition ( 100, "/" )   .
                    AddPartition ( 1900, "/data" ) ) ) .
            AddComputer (
            CComputer ( "imap.fit.cvut.cz" ) .
                    AddAddress ( "147.32.232.238" ) .
                    AddComponent ( CCPU ( 4, 2500 ) ) .
                    AddAddress ( "2001:718:2:2901::238" ) .
                    AddComponent ( CMemory ( 8000 ) ) );

    assert ( toString ( n ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  \\-Memory, 8000 MiB\n" );
    CNetwork x = n;
    auto c = x . FindComputer ( "imap.fit.cvut.cz" );
    assert ( toString ( *c ) ==
             "Host: imap.fit.cvut.cz\n"
             "+-147.32.232.238\n"
             "+-2001:718:2:2901::238\n"
             "+-CPU, 4 cores @ 2500MHz\n"
             "\\-Memory, 8000 MiB\n" );
    c -> AddComponent ( CDisk ( CDisk::MAGNETIC, 1000 ) .
            AddPartition ( 100, "system" ) .
            AddPartition ( 200, "WWW" ) .
            AddPartition ( 700, "mail" ) );

    assert ( toString ( x ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  +-Memory, 8000 MiB\n"
             "  \\-HDD, 1000 GiB\n"
             "    +-[0]: 100 GiB, system\n"
             "    +-[1]: 200 GiB, WWW\n"
             "    \\-[2]: 700 GiB, mail\n" );
    assert ( toString ( n ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  \\-Memory, 8000 MiB\n" );
  return 0;
}
#endif /* __PROGTEST__ */
